@echo off

del /f /s /q .\zip\app.hex
del /f /s /q .\zip\app_dfu_package.zip

copy /y .\..\..\ble_app_xiaoxiPy\pca10028\s130\iar\_build\ble_app_uart_pca10028_s130.hex .\zip\app.hex

echo Input you application-version number ......
set /p APPVER=
echo Your application-version is %APPVER%.

nrfutil.exe pkg generate --hw-version 51 --sd-req 0x87 --application-version %APPVER% --application .\zip\app.hex --key-file .\zip\private.pem app_dfu_package_v%APPVER%_%date:~5,2%%date:~8,2%_%time%.zip

set APPVER=

pause