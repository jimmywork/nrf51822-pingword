@echo off

del /f /s /q .\source\softdevice.hex
del /f /s /q .\source\app.hex
del /f /s /q .\source\boot.hex
del /f /s /q .\source\settings.hex
del /f /s /q .\source\settings-edited.hex
del /f /s /q .\sd_boot.hex
del /f /s /q .\sd_boot_app.hex
del /f /s /q .\sd_boot_app_settings.hex
copy /y .\..\..\..\..\components\softdevice\s130\hex\s130_nrf51_2.0.1_softdevice.hex .\source\softdevice.hex
copy /y .\..\..\ble_app_xiaoxiPy\pca10028\s130\iar\_build\ble_app_uart_pca10028_s130.hex .\source\app.hex

echo USE BOOT_DEBUG or BOOT(y/n):
set /p a=
if /i "%a%"=="n" (
		echo use production version
		copy /y .\..\..\bootloader_secure\pca10028\iar\_build\secure_dfu_secure_dfu_ble_s130_pca10028.hex .\source\boot.hex
	)
if /i "%a%"=="y" (
		echo use debug version
		copy /y .\..\..\bootloader_secure\pca10028_debug\iar\_build\secure_dfu_secure_dfu_ble_s130_pca10028_debug.hex .\source\boot.hex
	)

nrfutil.exe settings generate --family NRF51 --application .\source\app.hex --application-version 1 --bootloader-version 1 --bl-settings-version 1 .\source\settings.hex
start .\bin\dist\setting-edit.exe /wait
echo wait finish
pause>nul
::start python .\bin\setting-edit.py

::start
::tasklist /FI "IMAGENAME eq setting-edit.exe" > findant
::if %ERRORLEVEL% EQU 1 GOTO :start
::end
echo gen pack
path = %path%;.\bin\;.\source\;

mergehex.exe -m .\source\boot.hex .\source\softdevice.hex -o sd_boot.hex
mergehex.exe -m sd_boot.hex .\source\app.hex -o sd_boot_app.hex
mergehex.exe -m sd_boot_app.hex .\source\settings-edited.hex -o sd_boot_app_settings.hex

del /f /s /q .\zip\app.hex
copy /y .\..\..\ble_app_xiaoxiPy\pca10028\s130\iar\_build\ble_app_uart_pca10028_s130.hex .\zip\app.hex

echo Input you application-version number ......
set /p APPVER=
echo Your application-version is %APPVER%.

nrfutil.exe pkg generate --hw-version 51 --sd-req 0x87 --application-version %APPVER% --application .\zip\app.hex --key-file .\zip\private.pem app_dfu_package_v%APPVER%_%date:~5,2%%date:~8,2%.zip

set APPVER=

pause