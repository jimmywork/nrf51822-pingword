#define TestFrontData 0x1f
#define TestBackData  0


typedef enum
{
    TEST_STARTUP = 0,
	TEST_IDLE,
	TEST_AGING,
	TEST_AUDIO_PLAY,
	
} TEST_MODE_STATE;

void TestMode(TEST_MODE_STATE *psState);
uint8_t checkIfTestCard(void);

extern uint8_t tsetModeFlag;

