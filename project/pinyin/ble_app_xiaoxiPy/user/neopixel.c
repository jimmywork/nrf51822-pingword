/* Lava
 * 
 * WS2812B Tricolor LED (neopixel) controller
 *
 *
 * Example code:
 
	neopixel_strip_t m_strip;
	uint8_t dig_pin_num = 6;
	uint8_t leds_per_strip = 24;
	uint8_t error;
	uint8_t led_to_enable = 10;
	uint8_t red = 255;
	uint8_t green = 0;
	uint8_t blue = 159;
	neopixel_init(&m_strip, dig_pin_num, leds_per_strip);
	neopixel_clear(&m_strip);
	error = neopixel_set_color_and_show(&m_strip, led_to_enable, red, green, blue);
	if (error) {
		//led_to_enable was not within number leds_per_strip
	}
	//clear and remove strip
	neopixel_clear(&m_strip);
	neopixel_destroy(&m_strip);
 
 
 * For use with BLE stack, see information below:
	- Include in main.c
		#include "ble_radio_notification.h"
	- Call (see nrf_soc.h: NRF_RADIO_NOTIFICATION_DISTANCES and NRF_APP_PRIORITIES)
		ble_radio_notification_init(NRF_APP_PRIORITY_xxx,
		NRF_RADIO_NOTIFICATION_DISTANCE_xxx,
		your_radio_callback_handler);
	- Create 
		void your_radio_callback_handler(bool radio_active)
		{
			if (radio_active == false)
			{
				neopixel_show(&strip1);
				neopixel_show(&strip2);
				//...etc
			}
		}
	- Do not use neopixel_set_color_and_show(...) with BLE, instead use uint8_t neopixel_set_color(...);
 */
 
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <common.h>
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "neopixel.h"
#include "bsp.h"
/* rgb */
neopixel_strip_t rgbStrip;
static RGB_BLINK_PRAM rgbBlinkSet[RGB_MAX];

void neopixel_init(neopixel_strip_t *strip, uint8_t pin_num, uint16_t num_leds)
{
  strip->leds = (color_t*) malloc(sizeof(color_t) * num_leds);
  strip->pin_num = pin_num;
  strip->num_leds = num_leds;
  nrf_gpio_cfg_output(pin_num);
  NRF_GPIO->OUTCLR = (1UL << pin_num);
  neopixel_clear(strip);
  memset(rgbBlinkSet, 0, sizeof(rgbBlinkSet));
}

void neopixel_clear(neopixel_strip_t *strip)
{
  for (int i = 0; i < strip->num_leds; i++)
  {
    strip->leds[i].simple.g = 0;
    strip->leds[i].simple.r = 0;
    strip->leds[i].simple.b = 0;
  }
  neopixel_show(strip);
}

void neopixel_show(neopixel_strip_t *strip)
{
  const uint8_t PIN =  strip->pin_num;
  NRF_GPIO->OUTCLR = (1UL << PIN);
  nrf_delay_us(50);
  for (int i = 0; i < strip->num_leds; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      if ((strip->leds[i].grb[j] & 128) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 64) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 32) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 16) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 8) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 4) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 2) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}

      if ((strip->leds[i].grb[j] & 1) > 0)	{NEOPIXEL_SEND_ONE}
      else	{NEOPIXEL_SEND_ZERO}
    }
  }
}

uint8_t neopixel_set_color(neopixel_strip_t *strip, uint16_t index, uint8_t red, uint8_t green, uint8_t blue )
{
  if (index < strip->num_leds)
  {
    strip->leds[index].simple.r = red;
    strip->leds[index].simple.g = green;
    strip->leds[index].simple.b = blue;
  }
  else
    return 1;
  return 0;
}

uint8_t neopixel_set_multi_color_and_show(neopixel_strip_t *strip, uint16_t index, uint8_t red, uint8_t green, uint8_t blue)
{
  uint8_t i;
  //if (index < strip->num_leds)
  if (!(index &0x80))
  {
  	for(i=0;i<RGB_MAX;i++)
  	{
  		if(index&(1<<i))
  		{
  			strip->leds[i].simple.r = red;
		    strip->leds[i].simple.g = green;
		    strip->leds[i].simple.b = blue;
  		}
  	}
    
    neopixel_show(strip);	
  }
  else
    return 1;
  return 0;
}

uint8_t neopixel_set_color_and_show(neopixel_strip_t *strip, uint16_t index, uint8_t red, uint8_t green, uint8_t blue)
{
  if (index < strip->num_leds)
  {
    strip->leds[index].simple.r = red;
    strip->leds[index].simple.g = green;
    strip->leds[index].simple.b = blue;
    neopixel_show(strip);	
  }
  else
    return 1;
  return 0;
}

void neopixel_destroy(neopixel_strip_t *strip)
{
  free(strip->leds);
  strip->num_leds = 0;
  strip->pin_num = 0;
}
	
const color_t COL_LIB[COL_MAX]={
{0,0,0},
{128,0,0},//green
{0,128,0},//red
{0,0,128},//blue
{0,128,128},//purple
{63,63,0},//yellow
{255,255,255},//white
};
static uint8_t rgbUpdateFlag=0;

void SET_RGB(uint8_t sel, color_lib_t col_idx)
{
    uint8_t i;
	const color_t *col_sel;
	col_sel=&COL_LIB[col_idx];
	if (!(sel &0x80))
	{
		for(i=0;i<RGB_MAX;i++)
		{
			if(sel&(1<<i))
			{
				rgbStrip.leds[i].simple.r = col_sel->simple.r;
			    rgbStrip.leds[i].simple.g = col_sel->simple.g;
			    rgbStrip.leds[i].simple.b = col_sel->simple.b;
			}
		}
		rgbUpdateFlag = true;
	}
}


void RGB_UPDATE(void)
{
	if(rgbUpdateFlag)
	{
		rgbUpdateFlag=false;
		neopixel_show(&rgbStrip);
	}
}

void lightActiveCard(color_lib_t COL)
{
	uint8_t i,leds;
	SET_RGB(RGB_CARD_SEL, NONE);
	leds=0;
	for(i=0;i<CARDS_NUMBER;i++)
	{
    	if(bsp_button_is_pressed(i))
	    {
	    	leds|=idx2rgb(i);
	    }
	}
	SET_RGB(leds, COL);
}

//RGB灯闪烁函数

void RGB_BLINK(void)
{
    uint8_t i;
	static uint8_t tick=0;
	tick++;
    for (i = 0; i < RGB_MAX; i++)
    {

        if (rgbBlinkSet[i].RgbBlinkTimes == 0xff)
        {
            if (tick & rgbBlinkSet[i].RgbBlinkSpeed)
            {
                SET_RGB(idx2rgbN(i), rgbBlinkSet[i].RgbBlinkCol);
            }
            else
            {
                SET_RGB(idx2rgbN(i), NONE);
            }
        }
        else if (rgbBlinkSet[i].RgbBlinkTimes)
        {
            if (rgbBlinkSet[i].RgbBlinkTimes & rgbBlinkSet[i].RgbBlinkSpeed)
            {
                SET_RGB(idx2rgbN(i), NONE);
            }
            else
            {
                SET_RGB(idx2rgbN(i), rgbBlinkSet[i].RgbBlinkCol);
            }
            rgbBlinkSet[i].RgbBlinkTimes -= 1;
        }
    }
}

void SetRgbBlink(color_lib_t COL, uint8_t sel, uint8_t times, uint8_t speed)
{
    uint8_t i;
    for (i = 0; i < RGB_MAX; i++)
    {
        if (sel & (1 << i))
        {
            rgbBlinkSet[i].RgbBlinkCol = COL;
            if (times == 0xff)
            {
                rgbBlinkSet[i].RgbBlinkTimes = 0xff;
            }
            else
            {
                rgbBlinkSet[i].RgbBlinkTimes = 2 * times * speed;
            }
            rgbBlinkSet[i].RgbBlinkSpeed = speed;
        }
    }
}

void StopRgbBlink(uint8_t sel)
{
	uint8_t i;
	for (i = 0; i < RGB_MAX; i++)
    {
        if (sel & (1 << i))
        {
            rgbBlinkSet[i].RgbBlinkCol = NONE;
			rgbBlinkSet[i].RgbBlinkTimes=0;
			rgbBlinkSet[i].RgbBlinkSpeed=0;
        }
    }
}