//typedef enum
//{
//    PSTORAGE_CLEAR_OP,
//    PSTORAGE_CLEAR_OP_SUCCESS,
//    PSTORAGE_STORE_OP,
//    PSTORAGE_STORE_OP_SUCCESS,
//    PSTORAGE_UPDATE_OP,
//    PSTORAGE_UPDATE_OP_SUCCESS,
//    PSTORAGE_LOAD_OP,
//    PSTORAGE_LOAD_OP_SUCCESS,
//} ePstorageState_t;

typedef enum
{
    PAGE_USER=0,
    PAGE_SYS,
} page_sel_t;
	
//void FlashTest(void);
void flash_init(void);
void paramSave(uint8_t *source, uint16_t size,uint8_t diff);
ret_code_t paramLoad(uint8_t *source, uint16_t size,uint8_t diff);