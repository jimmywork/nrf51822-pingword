#ifndef NEOPIXEL_H
 #define NEOPIXEL_H


#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "nrf_gpio.h"

#define BlinkFast 0x02
#define BlinkSlow 0x04

typedef enum
{
    NONE,
    COL_GREEN,
    COL_RED,
    COL_BLUE,
    COL_PURPLE,
    COL_YELLOW,
    COL_WHITE,
    COL_MAX
} color_lib_t;

typedef struct
{
    color_lib_t RgbBlinkCol;
    uint8_t RgbBlinkTimes;
    uint8_t RgbBlinkSpeed;
} RGB_BLINK_PRAM;



typedef enum
{
    RGB_1,
    RGB_2,
    RGB_3,
    RGB_4,
    RGB_5,
    RGB_6,
    RGB_POWER,
    RGB_MAX,
} rgb_sel_t;

typedef enum
{
    RGB_1_SEL=0X01,
    RGB_2_SEL=0X02,
    RGB_3_SEL=0X04,
    RGB_4_SEL=0X08,
    RGB_5_SEL=0X10,
    RGB_6_SEL=0X20,
    RGB_POWER_SEL=0X40,
   	RGB_CARD_SEL=0x3f,
   	RGB_ALL_SEL=0x7f,
} rgb_pos_sel_t;

void RGB_UPDATE(void);

//These defines are timed specific to a series of if statements and will need to be changed
//to compensate for different writing algorithms than the one in neopixel.c
#define NEOPIXEL_SEND_ONE	NRF_GPIO->OUTSET = (1UL << PIN); \
		__ASM ( \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
			); \
		NRF_GPIO->OUTCLR = (1UL << PIN); \

#define NEOPIXEL_SEND_ZERO NRF_GPIO->OUTSET = (1UL << PIN); \
		__ASM (  \
				" NOP\n\t"  \
			);  \
		NRF_GPIO->OUTCLR = (1UL << PIN);  \
		__ASM ( \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
				" NOP\n\t" \
			);
		
extern void nrf_delay_us(uint32_t volatile number_of_us);

typedef union {
    struct {
            uint8_t g, r, b;
    }simple;
    uint8_t grb[3];
} color_t;

typedef struct {
	uint8_t pin_num;
	uint16_t num_leds;
	color_t *leds;
} neopixel_strip_t;

/**
  @brief Initialize GPIO and data location
  @param[in] pointer to Strip structure
	@param[in] pin number for GPIO
*/
void neopixel_init(neopixel_strip_t *strip, uint8_t pin_num, uint16_t num_leds);
	
/**
  @brief Turn all LEDs off
  @param[in] pointer to Strip structure
*/
void neopixel_clear(neopixel_strip_t *strip);

/**
  @brief Update strip with structure data
  @param[in] pointer to Strip structure
*/
void neopixel_show(neopixel_strip_t *strip);

/**
  @brief Write RGB value to LED structure
  @param[in] pointer to Strip structure
	@param[in] LED number (starting at 0)
	@param[in] red value
	@param[in] green value
	@param[in] blue value
  @retval 0 Successful write
  @retval 1 LED number is out of bounds
*/
uint8_t neopixel_set_color(neopixel_strip_t *strip, uint16_t index, uint8_t red, uint8_t green, uint8_t blue );


/**
  @brief Write RGB value to LED structure and update LED
  @param[in] pointer to Strip structure
	@param[in] LED number (starting at 0)
	@param[in] red value
	@param[in] green value
	@param[in] blue value
  @retval 0 Successful write
  @retval 1 LED number is out of bounds
*/
uint8_t neopixel_set_color_and_show(neopixel_strip_t *strip, uint16_t index, uint8_t red, uint8_t green, uint8_t blue);

/**
  @brief Clears structure data
  @param[in] pointer to Strip structure
*/
void neopixel_destroy(neopixel_strip_t *strip);
void SET_RGB(uint8_t SEL, color_lib_t COL);
void lightActiveCard(color_lib_t COL);
void RGB_BLINK(void);
void SetRgbBlink(color_lib_t COL, uint8_t sel, uint8_t times, uint8_t speed);
void StopRgbBlink(uint8_t sel);
extern neopixel_strip_t rgbStrip;
#endif