#include "common.h"
/* 长时间误操作计数 */
uint32_t noAct_1s_cont=0;
uint32_t noAct_100ms_cont=0;

static uint8_t paramModifyFlag=0;

void setModify(uint8_t a)
{
	paramModifyFlag|=a;
	NRF_LOG_DEBUG("set paramModifyFlag:%d\r\n",paramModifyFlag);
}

void info_record()
{
	if(paramModifyFlag)
	{
		NRF_LOG_DEBUG("paramModifyFlag:%d\r\n",paramModifyFlag);
	}
	if(paramModifyFlag&RECORD_USER_PARAM)
	{
		NRF_LOG_DEBUG("record user:%d\r\n",sizeof(USER_PARAM));
		paramSave((uint8_t *)&user_param,sizeof(USER_PARAM),PAGE_USER);
        paramModifyFlag&=(~RECORD_USER_PARAM);
		NRF_LOG_DEBUG("paramModifyFlag after:%d\r\n",paramModifyFlag);
	}
	if(paramModifyFlag&RECORD_SYS_PARAM)
	{
		NRF_LOG_DEBUG("record sys:%d\r\n",sizeof(SYS_PARAM));
		paramSave((uint8_t *)&sys_param,sizeof(SYS_PARAM),PAGE_SYS);
        paramModifyFlag&=(~RECORD_SYS_PARAM);
		NRF_LOG_DEBUG("paramModifyFlag after:%d\r\n",paramModifyFlag);
	}
	
}


void audio_push_number(uint16_t num)
{
	uint8_t u8Thousand,u8Hundred,u8Ten,u8One;
	u8Thousand = num / 1000;
    u8Hundred = num / 100 % 10;
    u8Ten = num / 10 % 10;
    u8One = num % 10;
    if (u8Thousand != 0)
    {
    	audioPush(audio_thousand(u8Thousand));
    }
    if (u8Hundred == 0)
    {
        if (u8Thousand)
        {
        	audioPush(audio_num(0));
        }
    }
    else
    {
    	audioPush(audio_hundred(u8Hundred));
    }
    if (u8Ten == 0)
    {
        if (u8One)
        {
            if (u8Hundred || u8Thousand)
            {
				audioPush(audio_one(u8One));
            }
            else
            {
                audioPush(audio_num(u8One));
            }
        }
        else
        {
            if (u8Hundred || u8Thousand)
            {
            }
            else
            {
                audioPush(audio_num(u8One));
            }
        }
    }
    else
    {
        if ((u8Hundred || u8Thousand) && (u8Ten == 1))
        {
			audioPush(audio_ten(u8One));
        }
        else
        {
			audioPush(audio_num(u8Ten * 10 + u8One));
        }
    }
}


INSERT_RESULT UpdateRgbReturn(bool nolight,bool chkRemove)
{
    uint8_t i;
    INSERT_RESULT ErrorFlag = InCorrect;
    uint8_t a,b,c;
    bool FinalCorrect = false;
	uint8_t lastInsertPosRelate=input_param.last_Insert_Pos-input_param.sta_pos;
	uint8_t firstBlank=0;
	if (!nolight)
    {
        lightActiveCard(InsertErrorCol);
    }
    if (input_param.word_str[0] == word_param.CurrentWord[0])
    {
    	firstBlank=1;
        if (((input_param.sta_pos + min(word_param.CurrentWordLen, CARDS_NUMBER)) > CARDS_NUMBER))
        {
            ErrorFlag = MoveForward;
            goto error;
        }
        if (!nolight)
        {
            SET_RGB(idx2rgb(input_param.sta_pos), InsertCorrectCol);
        }
    }
    else
    {
    	firstBlank=0;
        if (word_param.CurrentWordLen == 1)
        {
            ErrorFlag = InCorrect;
        }
        else
        {
            ErrorFlag = InsertFirst;
        }
        for (i = 1; i < input_param.word_str_len; i++)
        {
            if ((input_param.word_str[i] == word_param.CurrentWord[0]))
            {
                ErrorFlag = RemoveFrontError;
                break;
            }
        }
        goto error;
    }
    a = 0;
    b = 0;
	c = 0;
    FinalCorrect = false;
    if (input_param.word_str[0] == word_param.CurrentWord[0])
    {
        a++;
        for (i = 1; i < input_param.word_str_len; i++)
        {
            if (i < word_param.CurrentWordLen)
            {
                if ((input_param.word_str[i] == word_param.CurrentWord[i]))
                {
                    if (!nolight)
                    {
                        SET_RGB(idx2rgb(input_param.sta_pos+ i), InsertCorrectCol);
                    }
                    a++;
                    if (i == lastInsertPosRelate)
                    {
                        FinalCorrect = true;
                    }
                }
                else if (input_param.word_str[i] == '_')
                {
                	firstBlank=firstBlank?firstBlank:i;
                }
                else
                {
                    b++;
					c++;
					firstBlank=firstBlank?firstBlank:i;
                }
            }
            else
            {
                if (input_param.word_str[i] == '_')
                {
                
                }
                else
                {
                    b++;
                }
            }
        }
    }
    if ((a == min(word_param.CurrentWordLen, CARDS_NUMBER)) && (!b))
    {		
        if ((input_param.insert_Char == 0))
        {
            ErrorFlag = RemoveAndAllCorrect;
        }
        else
        {
            ErrorFlag = InsertAndAllCorrect;
        }
        if (!nolight)
        {
        	NRF_LOG_INFO("SetRgbBlink:%x\r\n",tFullWord[word_param.CurrentWordLen-1]>>input_param.sta_pos);
        	SetRgbBlink(InsertCorrectCol, tFullWord[word_param.CurrentWordLen-1]>>input_param.sta_pos, 3, BlinkFast);
        }
        goto correct;
    }
    else
    {
        if (b >= 2)
        {
            ErrorFlag = RemoveError;
        }
        else if (b == 1)
        {
            if ((FinalCorrect) || (lastInsertPosRelate == 0)||(!c))
            {
                FinalCorrect = false;
                ErrorFlag = RemoveError;
            }
        }
        else
        {
            ErrorFlag = PartialCorrect;
        }
    }
error:
	if(chkRemove)
	{
	    if (input_param.insert_Char == 0)
	    {
	        if (ErrorFlag != MoveForward)
	        {
	            ErrorFlag = NoSound;
	        }
	    }
	}
correct:
	input_param.error_Pos=firstBlank;
    return ErrorFlag;
}

void tipNextChar(uint8_t specific)
{
    uint8_t u8DiffPos,u8LetterIndex,u8colidx;
	u8DiffPos=(specific!=0xff)?specific:(input_param.error_Pos);
	u8LetterIndex=word_param.CurrentWord[u8DiffPos]-'A';
	if ((u8LetterIndex) && (u8LetterIndex <= 6)) //abcd efg
    {
        u8colidx = 0;
    }
    else if ((u8LetterIndex >= 7) && (u8LetterIndex <= 13)) //hijk lmn
    {
        u8colidx = 1;
    }
    else if ((u8LetterIndex >= 14) && (u8LetterIndex <= 19)) //opq rst
    {
        u8colidx = 2;
    }
    else//uvw xyz
    {
        u8colidx = 3;
    }
	if(u8DiffPos != 0xff)
	{
		audioPush(AUDIO_CARDIDX1+u8DiffPos);
		audioPush(AUDIO_CARDCOL1+u8colidx);
		audioPush(audio_26node[u8LetterIndex]);
	}
}

void audioPush_NodeType(uint16_t index)
{
    if (index < 24)
    {
        audioPush(AUDIO_FINDSHEN);
    }
    else if (index < 48)
    {
        audioPush(AUDIO_FINDYUN);
    }
    else if (index < 64)
    {
        audioPush(AUDIO_FINDZHENGTI);
    }
    else
    {
        audioPush(AUDIO_nLEARNPINYIN);
    }
}

void passStatusInit(EXAM_HIS_PARAM *in,uint8_t size)
{
	
	in->file.examPassStatus=in->StatusBuff;
	in->file.quesLib=in->quesBuff;
	memset(in->file.examPassStatus,0,sizeof(in->file.examPassStatus));
	memset(in->file.quesLib,0,sizeof(in->file.quesLib)*2);
	in->file.quesSize=size;
	in->file.useTimes=0;
	in->file.score=0;
}
	
void setPassStatus(EXAM_HIS_PARAM *in,uint8_t idx,PASS_STATUS pass)
{
	in->file.examPassStatus[idx/4]|=pass<<((idx%4)*2);
	if(pass==STATUS_PASS)
	{
		in->file.score++;
	}
}

static void playExamTime(uint32_t tick)
{
	uint32_t hours, mins, secs;
    secs = tick; //秒
    mins = secs / 60; //分
    hours = mins / 60; //时
    audioPush(AUDIO_EAXMTIMETOTAL);
    if (hours)
    {
        audio_push_number(hours);
        audioPush(AUDIO_HOUR);
    }
    if (mins % 60)
    {
        audio_push_number(mins % 60);
        audioPush(AUDIO_MINUTE);
    }
    if (secs % 60)
    {
        audio_push_number(secs % 60);
        audioPush(AUDIO_SEC);
    }
}

void playExamScore(uint32_t tick,uint8_t score)
{
	playExamTime(tick);
	audioPush(audio_score(score));
	switch(score)
	{
		case 6:
		case 7:
			audioPush(audio_exam_finish(3));
			break;
		case 8:
			user_param.Loc_Star+=1;
			paramModifyFlag|=RECORD_USER_PARAM;
			audioPush(audio_exam_finish(2));
			break;
		case 9:
			user_param.Loc_Star+=2;
			paramModifyFlag|=RECORD_USER_PARAM;
			audioPush(audio_exam_finish(1));
			break;
		case 10:
			user_param.Loc_Star+=3;
			paramModifyFlag|=RECORD_USER_PARAM;
			audioPush(audio_exam_finish(0));
			break;
		
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		default:
			audioPush(audio_exam_finish(4));
			break;
	}
}


PASS_STATUS getPassStatus(EXAM_HIS_PARAM *in,uint8_t idx)
{
	uint8_t res,pos;
	pos=(idx%4)*2;
	res=in->file.examPassStatus[idx/4]&(0x03<<pos);
	res>>=pos;
	return (PASS_STATUS)res;
}

void playCurrentQues(uint16_t currentQues)
{
	NRF_LOG_DEBUG("current ques: %x",currentQues);
	audioPush(AUDIO_FIND);
	audioPush_NodeType(currentQues);
	audioPush(word_param.AudioInfo[0]);
}

void power_off(void)
{
	u8PowerOnFlag=POW_BYEBYE;
	main_state=MAIN_STATE_POWER_OFF;
}

void cancel_power_off(void)
{
	u8PowerOnFlag=POW_ON;
	main_state=MAIN_STATE_MAIN_LOOP;
}

void record_reinit(void)
{
	app_int_fifo_init(&user_param.freeHisFifo, user_param.freeHisBuff, recordMax);
	passStatusInit(&user_param.loc_Exm_His,ExamQuesMax);
	passStatusInit(&user_param.ol_Exm_His,ExamQuesMax);
	passStatusInit(&user_param.ol_battle_His,ExamQuesMax);
}

void reset_record(void)
{
	sys_param.Sys_Vol=VolumeMin;
	memset(&user_param,0,sizeof(USER_PARAM));
    setModify(RECORD_USER_PARAM);
	record_reinit();
}

void audio_play_star(void)
{
	if(user_param.Loc_Star<=500)
	{
		audioPush(audio_flower(user_param.Loc_Star));
	}
	else
	{
		audio_push_number(user_param.Loc_Star);
		audioPush(AUDIO_STAR);
	}
}

#define special_py_max 15
const char *SPE_FREE_PY_MAP_CODE[15] =
{
    "UE",//==üe
    "Jv",//以下都是错误
    "Qv",
    "Xv",
    "Yv",
    "JvE",
    "QvE",
    "XvE",
    "YvE",
    "JvN",
    "QvN",
    "XvN",
    "YvN",
    "LUE",
    "NUE",
};

uint8_t isSpecialPY(char *pStr)
{
    const char *CurrentWordP;
    uint8_t i;
    for (i = 0; i < special_py_max; i++)
    {
        CurrentWordP = SPE_FREE_PY_MAP_CODE[i];

        if (strcmp(CurrentWordP, pStr) == 0) //当前拼入单词为特殊拼�?
        {
            switch (i)
            {
                case 0:
                    return 1;
                    break;

                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    return 2;
                    break;

                case 13:
                case 14:
                    return 3;
                    break;

                default:
                    return 0;
                    break;
            }
        }
    }
    return 0;
}

