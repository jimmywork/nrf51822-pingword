typedef enum
{
    FMS_STARTUP = 0,
    FMS_CHECK_USART_CLEAR,
    FMS_CHECK_USART,
    FMS_PLAY_WORD,
    FMS_MEDAL,
    FMS_AUDIO_PLAY,
}FREE_MODE_STATE;

extern void FreeMode(FREE_MODE_STATE *psState);
extern bool modeFreePlaying;