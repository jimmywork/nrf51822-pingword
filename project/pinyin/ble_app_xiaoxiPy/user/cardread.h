//#include "app_fifo.h"

#define SEL_NUMBER 3
#define IRS_NUMBER 5
#define CARDS_NUMBER 6

typedef enum
{
	STATUS_NONE=0,
    STATUS_INSERT,
    STATUS_REMOVE,
} input_state_t;

typedef struct
{
//uint16_t currentResPos;
//uint16_t KeyDetectOnline;
//uint8_t lastInsertPosRelate;
uint8_t Insert_Pos;
//uint8_t left;
//uint8_t right;
uint8_t error_Pos;
char charBuff[CARDS_NUMBER];
char word_str[CARDS_NUMBER+1];
uint8_t sta_pos;
uint8_t last_Insert_Pos;
uint8_t word_str_len;
char insert_Char;
input_state_t dataflag;
} INPUT_PARAM;

extern INPUT_PARAM input_param;
void card_init(void);
void checkCardInput(void);
void user_card_sel(uint8_t card_idx);
void user_card_unsel(void);
uint8_t card_data_read(void);