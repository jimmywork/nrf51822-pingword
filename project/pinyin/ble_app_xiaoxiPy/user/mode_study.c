#include "common.h"

void StudyMode(STUDY_MODE_STATE *psState)
{
    static STUDY_MODE_STATE study_state_ret=ELMS_STARTUP;
	static INSERT_RESULT currentResult;
	static uint16_t NewWordLearnedCont=0;
	static uint16_t tick=0;
	static uint16_t *learn_idx;
	static uint16_t *progress;
	static uint16_t wordMax;
	static bool pySpecialFlag=false;
    switch (*psState)
    {
        case ELMS_STARTUP:
	   	 	NRF_LOG_INFO("study mode\r\n");
	    	audio_interrupt_flag=all_interrupt;
		
			if(user_mode_last==MODE_STUDY)
			{
				audioPush(AUDIO_STUDYMODE);
				wordMax=AUDIO_PY_MAP_CODE_LEN;
			}
			else
			{
				audioPush(AUDIO_ENG_APP_MODE);
				audio_push_number(user_param.Ble_Study_Num);
				audioPush(AUDIO_wordCont2);
				wordMax=user_param.Ble_Study_Num;
			}
            *psState=ELMS_AUDIO_PLAY;
            study_state_ret=ELMS_CHECK_USART_CLEAR;
            break;
            
        case ELMS_CHECK_USART_CLEAR:
	    	audio_interrupt_flag=all_interrupt;
            *psState=ELMS_GET_CUR_WORD;
            break;

		case ELMS_GET_CUR_WORD:
			audio_interrupt_flag=key_interrupt;
			SET_RGB(RGB_CARD_SEL, NONE);
			if(user_mode_last==MODE_STUDY)
			{
				learn_idx=&user_param.Loc_Study_Index;
				progress=&user_param.Loc_Study_Index;
			}
			else
			{
				learn_idx=&user_param.Ble_Study[user_param.Ble_Study_Index];
				progress=&user_param.Ble_Study_Index;
			}
			if(getCurrentPyLearn(*learn_idx))
			{
				pySpecialFlag=false;
				if(!strcmp("VE",word_param.CurrentWord))
				{
					pySpecialFlag=true;
				}
				audioPush(AUDIO_FIND);
				audioPush_NodeType(*learn_idx);
				audioPush(word_param.AudioInfo[0]);
				*psState=ELMS_AUDIO_PLAY;
            	study_state_ret=ELMS_FIRSTCOLOR;
			}
			break;
			
		case ELMS_FIRSTCOLOR:
			audio_interrupt_flag=all_interrupt;
			UpdateRgbReturn(true,true);
			mp3_Delay(10);
			tipNextChar(0xff);
			*psState=ELMS_AUDIO_PLAY;
            study_state_ret=ELMS_CHECK_USART;
			break;
		
        case ELMS_CHECK_USART:
			audio_interrupt_flag=all_interrupt;
            if(input_param.dataflag==STATUS_INSERT)
            {
            	input_param.dataflag=STATUS_NONE;
            	//if(getWordStr())
            	if (pySpecialFlag)
                {
                    if (input_param.word_str[0] == 'U')
                    {
                        input_param.word_str[0] = 'V';
                    }
                }
				if(1)	
            	{
					if(isPyCard(input_param.insert_Char))
					{
						currentResult=UpdateRgbReturn(false,true);
						*psState=ELMS_AUDIO_PLAY;
            			study_state_ret=ELMS_CHECK_USART;
						NRF_LOG_INFO("currentResult:%d\r\n",currentResult);
						switch (currentResult)
			            {
			                case InsertAndAllCorrect:
								audioPush(audio_26node[input_param.insert_Char-'A']);
								audioPush(AUDIO_FINDCORRECT);
			                case RemoveAndAllCorrect:
								audioPush(AUDIO_DINGDONG);
			                    audio_interrupt_flag=none;
			                    study_state_ret = ELMS_WORD_CORRECT;
			                    break;

			                case MoveForward:
			                    audioPush(AUDIO_MOVEFORWARD);
			                    break;

			                case InCorrect:
								audioPush(AUDIO_ERROR);
			                    break;
							
			                case RemoveError:
							case RemoveFrontError:
			                    audioPush(AUDIO_REMOVEERROR);
			                    break;

			                case InsertFirst:
			                    audioPush(AUDIO_INSERTFIRSTNODE);
			                    break;
							case PartialCorrect:
								audioPush(audio_26node[input_param.insert_Char-'A']);
								audioPush(AUDIO_FINDCORRECT);
								break;

			                case NoSound:
			                default:
								*psState = ELMS_CHECK_USART;
			                    break;
			            }
						currentResult=UnKnown;
					}
            		else
            		{
            			*psState = ELMS_CHECK_USART;
            		}
            	}
				else
				{
				}
            }
            break;  
			
		case ELMS_FIND_LETTER:
			if(getWordStr())//不排除有拔出，需重新获取
			{
				currentResult=UpdateRgbReturn(false,true);
				switch (currentResult)
			    {
			         case InsertAndAllCorrect:
			         case RemoveAndAllCorrect:
						audioPush(AUDIO_DINGDONG);
			             audio_interrupt_flag=none;
			             study_state_ret = ELMS_WORD_CORRECT;
			             break;

			         case MoveForward:
			             audioPush(AUDIO_MOVEFORWARD);
			             break;

					case InCorrect:
			        case RemoveError:
					case RemoveFrontError:
			             audioPush(AUDIO_REMOVEERROR);
			             break;

			         case InsertFirst:
			             audioPush(AUDIO_INSERTFIRSTNODE);
			             break;
					 
					case PartialCorrect:
			        case NoSound:
			        default:
			             break;
			     }
				currentResult=UnKnown;
			}
			audioPush_NodeType(*learn_idx);
			audioPush(word_param.AudioInfo[0]);
			tipNextChar(0xff);
			*psState=ELMS_AUDIO_PLAY;
            study_state_ret=ELMS_CHECK_USART;
			break;
			
        case ELMS_WORD_CORRECT:
			audio_interrupt_flag=none;
			audioPush(word_param.AudioInfo[0]);
			audioPush(AUDIO_PINGSUCCESS);
			audioPush(AUDIO_Encourage1+rand() % 3);
			audioPush(AUDIO_LETSREAD);
	        setSubWordPlayList();
			*psState=ELMS_AUDIO_PLAY;
			study_state_ret=ELMS_MEDAL;
            break;
            
        case ELMS_MEDAL:
           if (++NewWordLearnedCont >= 5)
           {
               NewWordLearnedCont = 0;
               user_param.Loc_Star += 1;
               setModify(RECORD_USER_PARAM);
				audioPush(AUDIO_EncourageAndFlower1+rand() % 3);
				audio_play_star();
               study_state_ret = ELMS_WAIT5S;
               *psState = ELMS_AUDIO_PLAY;
           }
           else
           {
               //新词，未满5个
               *psState = ELMS_WAIT5S;
           }
		   tick=0;
           break;

		case ELMS_WAIT5S:
			audio_interrupt_flag=key_interrupt;
			if( tick++ > 500 )//5S后自动跳到下个单词
            {
              tick = 0;
			  *psState = ELMS_NEXT;
            }//在外部判断橙色键是否按下
			break;
			
		case ELMS_REREAD:
			audio_interrupt_flag=none;
			audioPush(AUDIO_LETSREAD);
	        setSubWordPlayList();
			*psState=ELMS_AUDIO_PLAY;
			study_state_ret=ELMS_WAIT5S;
            break;
			
		case ELMS_NEXT:
            *progress+=1;
			if(*progress>=wordMax)
			{
				*progress=0;
				*psState = ELMS_STARTUP;
			}
			else
			{
				*psState = ELMS_GET_CUR_WORD;
			}
			setModify(RECORD_USER_PARAM);
            break;
			
        case ELMS_AUDIO_PLAY:
          if(mp3RecStatus==empty)
          {
            *psState=study_state_ret;
			tick = 0;
          }
          break;
          
        default:
            *psState = ELMS_STARTUP;
            break;
    }
}