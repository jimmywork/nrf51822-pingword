#define UART_TX_BUF_SIZE                64                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                16                                         /**< UART RX buffer size. */


void sleep_mode_enter(void);
void user_phy_init(void);
void services_init(void);
void buttons_init(void);
void setVolUM(uint8_t plusOrMinus);

