#include "common.h"
#include "app_uart.h"
#include "bsp.h"



#define user_cmd_Buffer_size  128

APP_TIMER_DEF(m_mp3_uart_timer_id);
app_int_fifo_t user_cmd_fifo;
mp3_rec_state_t mp3RecStatus=idle;
uint16_t mp3_pre_play=0;
uint8_t audio_interrupt_flag=0;
uint8_t force_interrupt_flag=0;
static uint8_t audio_clear=0;

/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to
 *          a string. The string will be be sent over BLE when the last character received was a
 *          'new line' i.e '\r\n' (hex 0x0D) or if the string has reached a length of
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
static void uart_event_handle(app_uart_evt_t * p_event)
{
    //static uint8_t stateCode;
    uint8_t resCode=0xff;
    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&resCode));
            app_timer_stop(m_mp3_uart_timer_id);//防止超时标志置位影响下次操作
            NRF_LOG_DEBUG("mp3RecStatus:%d\r\n",mp3RecStatus);
            if(mp3RecStatus==standby)
            {
               if(resCode==0x00)
               {
                 mp3_en();
                 mp3RecStatus=busy;
               }
               else if(resCode==0x05)
               {
                 mp3RecStatus=fail;
               }
               else
               {
                 mp3RecStatus=fail;
               }
            }
			else if(mp3RecStatus==cmd_sta)
			{
				if(resCode==0x00)
                {
                  mp3RecStatus=ok;
				  if(u8PowerOnFlag==POW_STANDBY)
				  {
				  	u8PowerOnFlag=POW_ON;
				  }
                }
                else
                {
                  mp3RecStatus=fail;
                }
			}
			else if((mp3RecStatus==idle)||(mp3RecStatus==empty))
			{
				if(usbConMode!=usb_chking)
				{
					if(resCode==0xca)
					{
						NRF_LOG_DEBUG("usb_chking\r\n");
						usbConMode=usb_chking;
					}
					else
					{
						NRF_LOG_DEBUG("usb_error\r\n");
						usbConMode=usb_error;
					}
				}
				else
				{
					if(resCode&0x08)
					{
						NRF_LOG_DEBUG("usb_driver\r\n");
						usbConMode=usb_driver;
					}
					else
					{
						NRF_LOG_DEBUG("usb_driver\r\n");
						usbConMode=usb_normal;
					}
				}
			}
			
            NRF_LOG_INFO("r:%d\r\n",resCode);
            break;

        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_INFO("\r\nAPP_UART_COMMUNICATION_ERROR\r\n");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_INFO("\r\nAPP_UART_FIFO_ERROR\r\n");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}
/**@snippet [Handling the data received over UART] */


/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */

static void user_uart_check_handler(void * p_context)
{
    if((mp3RecStatus==standby)||(mp3RecStatus==cmd_sta)||(mp3RecStatus==delay_sta))
      mp3RecStatus=timeout;
    NRF_LOG_INFO("mp3-rec timeout\r\n");
}

void uart_init(void)
{
    uint32_t err_code;
    const app_uart_comm_params_t comm_params =
    {
        RX_PIN_NUMBER,
        TX_PIN_NUMBER,
        RTS_PIN_NUMBER,
        CTS_PIN_NUMBER,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud9600
    };

    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);
    //创建串口应答检测定时器
    
    err_code = app_timer_create(&m_mp3_uart_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                user_uart_check_handler);
    APP_ERROR_CHECK(err_code);
}
/**@snippet [UART Initialization] */

void AUDIO_CMD(uint8_t cmd,uint8_t * pack,uint8_t len,uint32_t timeOut)
{
  uint8_t i,offset,u8Msg[16],sum;
  offset=0;
  sum=0;
  u8Msg[offset++]=0x7e;
  u8Msg[offset++]=len+3;
  sum+=len+3;
  u8Msg[offset++]=cmd;
  sum+=cmd;
  for(i=0;i<len;i++)
  {
    u8Msg[offset++]=pack[i];
    sum+=pack[i];
  }
  u8Msg[offset++]=sum;
  u8Msg[offset++]=0xef;
  SEGGER_RTT_printf_BUFFER("wt2003 cmd",u8Msg,offset);
  for (i = 0; i < offset; i++)
  {
      while (app_uart_put(u8Msg[i]) != NRF_SUCCESS);
  }
  app_timer_start(m_mp3_uart_timer_id, APP_TIMER_TICKS(timeOut, APP_TIMER_PRESCALER), NULL);
}

uint8_t mp3_Play(uint16_t index)
{
    char buffer[16];
    int charLen;
    uint8_t u8Dir;
    uint16_t u8File;
    
    if (!index) //为0说明音频不存在
    {
        return false;
    }
    u8Dir=(index/1000)%0x10;//<7-F:提示音>7 8 9 a b c d e f
    if (u8Dir >= 6) //普通音频
    {
        u8File=index%1000;
        charLen=sprintf(buffer,"SYS%02d%03d",u8Dir-6,u8File);
    }
    else
    {
        u8Dir  = (index - 1) / 200 + 1;
        u8File = index;
        charLen=sprintf(buffer,"AU%03d%04d",u8Dir,u8File);
    }
    if(charLen>0)
    {
      AUDIO_CMD(WT_PlaySpecific,(uint8_t *)buffer,charLen,2000);
      return true;
    }
    return false;
}

void mp3_Delay(uint8_t u100ms)//ccdd ddd
{
    uint16_t cmd;
    
    cmd=code_cmd;
    cmd<<=2;
    cmd|=cmd_delay;
    cmd<<=12;
    cmd+=u100ms;
    app_int_fifo_put(&user_cmd_fifo, cmd);
}

void mp3_rgb_push(uint8_t sel,color_lib_t col)//ccdd ddd
{
    uint16_t cmd;
    
    cmd=code_rgb;
    cmd<<=6;
    cmd|=sel&0x3f;
    cmd<<=8;
    cmd+=col;
    app_int_fifo_put(&user_cmd_fifo, cmd);
}

void mp3_SetVolume(uint8_t u8Volume)//ccdd ddd
{
    uint16_t vol;
//    uint16_t return_val;
    vol=code_cmd;
    vol<<=2;
    vol|=cmd_setvol;
    vol<<=12;
    vol+=(u8Volume > 31)?31:u8Volume;
    NRF_LOG_INFO("set vol: %04x\r\n",vol);
    app_int_fifo_put(&user_cmd_fifo, vol);
//    app_int_fifo_get(&user_cmd_fifo, &return_val);
//    NRF_LOG_INFO("get %04x\r\n",return_val);
}

void mp3_Stop(void)
{
    uint16_t cmd;
    cmd=code_cmd;
    cmd<<=2;
    cmd|=cmd_stop;
    cmd<<=12;
    app_int_fifo_put(&user_cmd_fifo, cmd);
    //AUDIO_CMD(WT_StopPlaying,NULL,0,100);
}

void mp3_CheckUsb(void)
{
    uint16_t cmd;
    cmd=code_cmd;
    cmd<<=2;
    cmd|=cmd_chkusb;
    cmd<<=12;
    app_int_fifo_put(&user_cmd_fifo, cmd);
}

static uint16_t mp3_code_Buffer[user_cmd_Buffer_size];
void mp3FifoInit(void)
{
  memset(mp3_code_Buffer,0,user_cmd_Buffer_size*sizeof(uint16_t));
  app_int_fifo_init(&user_cmd_fifo, mp3_code_Buffer, user_cmd_Buffer_size);
}

void audioPush(uint16_t code)
{
  //NRF_LOG_INFO("audio Push: %04x",code);
  app_int_fifo_put(&user_cmd_fifo, code);
}

uint8_t is_aduio_empty(void)
{
  return fifo_len_get(&user_cmd_fifo)?0:1;	
}

//audio_Interrupt_Reset(insert_interrupt)
//audio_Interrupt_Reset(key_interrupt)

void audio_stop_clear(void)
{
	audio_clear=1;
	app_int_fifo_flush(&user_cmd_fifo);
}

void audio_Interrupt_Reset(uint8_t inter_type)
{
	if(audio_interrupt_flag&inter_type)
	{
		audio_stop_clear();
		lightActiveCard(COL_PURPLE);
	}
}

void audio_force_Interrupt(void)
{
	force_interrupt_flag=1;
    audio_stop_clear();
}

void user_interface(void)
{
    uint16_t return_val;
    uint8_t type,cmd,rVal;
    static uint16_t pre_fifo_get=0;
    static uint8_t retry=0;
	//单词串口通讯结束，回归初始状态
	if(mp3RecStatus==ok)
    {
      mp3RecStatus=idle;
    }
    else if((mp3RecStatus==fail)||(mp3RecStatus==timeout))
    {
      if(retry)
      {
        retry=0;
        app_int_fifo_put(&user_cmd_fifo, pre_fifo_get);
      }
      mp3RecStatus=idle;
    }
	//音频打断判断
	if((audio_interrupt_flag&insert_interrupt)||(audio_interrupt_flag&key_interrupt)||force_interrupt_flag)
    {
    	force_interrupt_flag=0;
      	if(audio_clear)
      	{
      		audio_clear=0;
			if(mp3RecStatus==play_sta)
	      	{
	      		//此时播放指令应答接收完毕，定时器已关
	      		//只负责打断，不清除数据
	      		mp3_dis();
	      		AUDIO_CMD(WT_StopPlaying,NULL,0,100);
	      	}
			else if(mp3RecStatus==delay_sta)
			{
				app_timer_stop(m_mp3_uart_timer_id);//防止超时标志置位影响下次操作
				mp3RecStatus=idle;
			}
			else
			{
				NRF_LOG_DEBUG("un break %d\r\n",mp3RecStatus);
			}
      	}      	
    }
	else
	{
		audio_clear=0;
	}
	//音频播放结束判断
	if(mp3RecStatus==busy)
    {
      if(nrf_gpio_pin_read(GPIO_MP3_BUSY))
	  {
	  	mp3RecStatus=play_sta;
	  }
    } 
	else if(mp3RecStatus==play_sta)
	{
		if(!nrf_gpio_pin_read(GPIO_MP3_BUSY))
	    {
	      mp3_dis();
	      mp3RecStatus=ok;
	    }
	}
    if((mp3RecStatus==idle)||(mp3RecStatus==empty))//接收或超时才可以获取下条指令
    {
	    if(app_int_fifo_get(&user_cmd_fifo, &return_val)==NRF_SUCCESS)
	    {
	      mp3_pre_play=return_val;
	      //NRF_LOG_INFO("fifo get: %04x",return_val);
	      retry=0;
	      pre_fifo_get=return_val;
	       //根据指令序号来判断<亮灯信号><音频播放指令><延时指令>
	      type=(uint8_t)((return_val&0xc000)>>14);
	      NRF_LOG_INFO("type:%d,val:%04x\r\n",type,return_val);
	      switch(type)
	      {
	        case code_mp3:
	          mp3RecStatus=standby;	
	          mp3_Play(return_val&0x3fff);
	          break;
	          
	        case code_cmd:
	          cmd=(uint8_t)((return_val&0x3000)>>12);
	          rVal=(uint8_t)(return_val&0x00ff);
	          switch(cmd)
	          {
	            case cmd_setvol:
				  mp3RecStatus=cmd_sta;	
	              AUDIO_CMD(WT_SetVol,&rVal,1,500);
	              retry=1;
	              break;
				  
	            case cmd_stop:
				  mp3RecStatus=cmd_sta;	
	              AUDIO_CMD(WT_StopPlaying,NULL,0,100);
	              break;
	            case cmd_delay:
				  mp3RecStatus=delay_sta;	
	              app_timer_start(m_mp3_uart_timer_id, APP_TIMER_TICKS(rVal*100, APP_TIMER_PRESCALER), NULL);
	              break;
				case cmd_chkusb:
				  AUDIO_CMD(WT_CheckDevStatus,NULL,0,100);
				  break;
	          }
	          break;
	          
	        case code_rgb:
		  	  cmd=(uint8_t)((return_val&0x3F00)>>8);
	          rVal=(uint8_t)(return_val&0x00ff);
	          SET_RGB(cmd, (color_lib_t)rVal);
	          mp3RecStatus=ok;
	          break;
	      }
	    }
		else
		{
			//NRF_LOG_INFO("audio fifo empty\r\n");
			mp3RecStatus=empty;
			if(u8PowerOnFlag==POW_BYEBYE)
			{
				u8PowerOnFlag=POW_OFF;
				SET_RGB(RGB_ALL_SEL,NONE);
			}
		}
    }
}

uint8_t isVolAudio(uint16_t idx)
{
	uint16_t tmp;
	tmp=idx-SYS_AUDIO_BASE;
	switch(tmp)
	{
	case 6:
        case 7:
        case 64:
        case 65:
        case 66:
        case 67:
        case 68:
			return true;
			break;
		default:
			return false;
			break;
	}
}