#include "common.h"
#include "bsp.h"
//裸板通过USB供电，
//通过复位键进入测试模式
//上电自动测试当前光电管状态
//按键切换群灯颜色
//按下复位键开启老化测试
//插入与初始状态全反即退出测试模式

static void ReadIRandLight(void)
{
    uint8_t i;
    uint8_t tmp;
    for (i = 0; i < CARDS_NUMBER; i++)
    {
        user_card_sel(i);
		nrf_delay_ms(200);
		tmp=card_data_read();
		user_card_unsel();
		nrf_delay_ms(200);
		while (app_uart_put(tmp) != NRF_SUCCESS);
		SET_RGB(idx2rgb(i), (tmp == TestBackData) ? COL_GREEN : COL_RED);
    }
}
const char FRONT_STR[]="111111";
const char BACK_STR[] ="222222";

uint8_t tsetModeFlag=0;
uint8_t checkIfTestCard(void)
{
	if(getWordStr()==CARDS_NUMBER)//仅需在插入后执行一次，其他位置可免除
	{
		if(!strcmp(input_param.word_str,FRONT_STR))
		{
			NRF_LOG_DEBUG("frot-pass:%d",tsetModeFlag);
			SET_RGB(RGB_POWER_SEL, COL_RED);
			tsetModeFlag |= 0x01;
		}
		else if(!strcmp(input_param.word_str,BACK_STR))
		{
			NRF_LOG_DEBUG("back-pass:%d",tsetModeFlag);
			SET_RGB(RGB_POWER_SEL, COL_GREEN);
			tsetModeFlag |= 0x02;
		}
		return tsetModeFlag;
	}
	return 0;
}

void TestMode(TEST_MODE_STATE *psState)
{
    static TEST_MODE_STATE test_state_ret=TEST_STARTUP;
	static uint8_t song_Idx;
    switch (*psState)
    {
        case TEST_STARTUP:
            NRF_LOG_INFO("free mode\r\n");
			audio_interrupt_flag=insert_interrupt;
			if(isUSBInsert())
			{
				ReadIRandLight();
				*psState=TEST_IDLE;
			}
			else
			{
				audioPush(AUDIO_TEST_MODE);
				*psState=TEST_AUDIO_PLAY;
				test_state_ret = TEST_IDLE;
			}
            
            break;
			
		case TEST_IDLE:
			if(bsp_button_is_pressed(BSP_EVENT_KEY_POWER-BSP_EVENT_KEY_INS_1))
			{
				u8PowerOnFlag=POW_OFF;
			}
			if(input_param.dataflag==STATUS_INSERT)//插入正反黑白则进入正常运行模式
            {
            	input_param.dataflag=STATUS_NONE;
				if (checkIfTestCard() == 0x03)
				{
					tsetModeFlag=0;
					SET_RGB(RGB_ALL_SEL, COL_BLUE);
					main_state=MAIN_STATE_MAIN_STARTUP;
				}
			}
			break;
			
		case TEST_AGING:
			noAct_1s_cont=0;
			if(mp3RecStatus==empty)
			{
				if (song_Idx++ >= SongMax)
	            {
	                song_Idx = 0;
	            }
				audioPush(audio_song_sel(song_Idx));
			}
			break;
			
		case TEST_AUDIO_PLAY:
          if(mp3RecStatus==empty)
          {
            *psState=test_state_ret;
          }
          break;
          
        default:
            *psState = TEST_STARTUP;
            break;
   	}
}

