#include "common.h"


uint16_t exam_currentQues=0;

EXAM_HIS_PARAM curHisParam;


void generateSeed(uint16_t *in,uint16_t range)
{
	uint8_t i,j,keepGoing;
	uint16_t tmp;
	for(i=0;i<sizeof(in)*sizeof(uint16_t);i++)
	{
		do
		{
			keepGoing=0;
			tmp=rand()%range;
			for(j=0;j<i;j++)
			{
				if(in[j]==tmp)
				{
					keepGoing=1;
				}
			}	
		}
		while(keepGoing);
		NRF_LOG_DEBUG("rand:%x",tmp)
		in[i]=tmp;
	}
}


void ExamMode(EXAM_MODE_STATE *psState)
{
    static EXAM_MODE_STATE exam_state_ret=EXM_STARTUP;
	INSERT_RESULT currentResult;
	
	static uint8_t quesCont=0;
	static uint8_t continueFlag=0;//新一轮

	static uint8_t staTimeContFlag=0;
	static uint32_t tick=0;

	if(staTimeContFlag)
	{
          tick++;
	}
    switch (*psState)
    {
        case EXM_STARTUP:
	   	 	NRF_LOG_INFO("exam mode\r\n");
			srand(noAct_100ms_cont);
	    	continueFlag=0;
		case EXM_CONTINUE:
			audio_interrupt_flag=all_interrupt;
			quesCont=0;
			tick=0;
			passStatusInit(&curHisParam,ExamQuesMax);

			switch(user_mode_last)
			{
				case MODE_EXAM:
					generateSeed(curHisParam.file.quesLib,AUDIO_PY_MAP_CODE_LEN);//生成随机题库
					SEGGER_RTT_printf_BUFFER("quesLib",(uint8_t *)curHisParam.file.quesLib,20);
					SEGGER_RTT_printf_BUFFER("quesBuff",(uint8_t *)curHisParam.quesBuff,20);
					break;
				
				case MODE_OL_BATTLE:
				case MODE_OL_EXAM:
					memcpy(curHisParam.file.quesLib,user_param.Ble_Exam,sizeof(user_param.Ble_Exam));
					break;
			}
			
			switch(user_mode_last)
			{
				case MODE_EXAM:
					if(continueFlag){
						audioPush(AUDIO_EXAM_MODE2);
					}
					else{
						audioPush(AUDIO_EXAM_MODE);	
					}
					break;
				case MODE_OL_EXAM:
					audioPush(AUDIO_ENG_EXAM_APP_MODE);
					break;
				case MODE_OL_BATTLE:
					audioPush(AUDIO_Onlinechallenge);
					break;
			}
			
            *psState=EXM_AUDIO_PLAY;
            exam_state_ret=EXM_CHECK_USART_CLEAR;
            break;
            
        case EXM_CHECK_USART_CLEAR:
	    	audio_interrupt_flag=all_interrupt;
            *psState=EXM_GET_CUR_WORD;
            break;

		case EXM_GET_CUR_WORD:
			audio_interrupt_flag=none;
			SET_RGB(RGB_CARD_SEL, NONE);
			exam_currentQues=curHisParam.file.quesLib[quesCont];
			if(getCurrentPyLearn(exam_currentQues))
			{
				audioPush(audio_ques_idx(quesCont));
				playCurrentQues(exam_currentQues);
				*psState=EXM_AUDIO_PLAY;
				exam_state_ret=EXM_FIRSTCOLOR;
			}
			staTimeContFlag=1;
			break;
			
		case EXM_FIRSTCOLOR:
			audio_interrupt_flag=all_interrupt;
			UpdateRgbReturn(true,true);
			mp3_Delay(10);
			tipNextChar(0xff);
			*psState=EXM_AUDIO_PLAY;
			exam_state_ret=EXM_CHECK_USART;
			noAct_1s_cont=0;
			break;
		
        case EXM_CHECK_USART:
			audio_interrupt_flag=all_interrupt;
            if(input_param.dataflag==STATUS_INSERT)
            {
            	input_param.dataflag=STATUS_NONE;
				if(1)
            	//if(getWordStr())
            	{
					if(isPyCard(input_param.insert_Char))
					{
						currentResult=UpdateRgbReturn(false,true);
						*psState=EXM_AUDIO_PLAY;
            			exam_state_ret=EXM_CHECK_USART;
						NRF_LOG_INFO("currentResult:%d\r\n",currentResult);
						switch (currentResult)
			            {
			                case InsertAndAllCorrect:
								audioPush(audio_26node[input_param.insert_Char-'A']);
								audioPush(AUDIO_FINDCORRECT);
			                case RemoveAndAllCorrect:
								audioPush(AUDIO_DINGDONG);
			                    audio_interrupt_flag=none;
								exam_state_ret=EXM_WORD_CORRECT;
			                    break;

			                case MoveForward:
			                    audioPush(AUDIO_MOVEFORWARD);
			                    break;

			                case InCorrect:
								audioPush(AUDIO_ERROR);
			                    break;
							
			                case RemoveError:
							case RemoveFrontError:
			                    audioPush(AUDIO_REMOVEERROR);
			                    break;

			                case InsertFirst:
			                    audioPush(AUDIO_INSERTFIRSTNODE);
			                    break;
							case PartialCorrect:
								audioPush(audio_26node[input_param.insert_Char-'A']);
								audioPush(AUDIO_FINDCORRECT);
								break;

			                case NoSound:
			                default:
								*psState = EXM_CHECK_USART;
			                    break;
			            }
						currentResult=UnKnown;
					}
            		else
            		{
						audioPush(AUDIO_NO_PY_CARD);
            			*psState=EXM_AUDIO_PLAY;
            			exam_state_ret=EXM_CHECK_USART;
            		}
            	}
            }
            break;  

		case EXM_NOACT:
			*psState=EXM_AUDIO_PLAY;
            exam_state_ret=EXM_CHECK_USART;
			break;
			
		case EXM_JUMOPVER:
			audio_interrupt_flag=insert_interrupt;
			setPassStatus(&curHisParam, quesCont, STATUS_JUMP);
			quesCont++;
			audioPush(AUDIO_JUMPOVER);
			if(quesCont<ExamQuesMax)
			{
				audioPush(AUDIO_examContinue);
				*psState=EXM_AUDIO_PLAY;
				exam_state_ret=EXM_CHECK_USART_CLEAR;
			}
			else
			{
				*psState=EXM_AUDIO_PLAY;
				exam_state_ret=EXM_MEDAL;
			}
			staTimeContFlag=0;
			break;
			
        case EXM_WORD_CORRECT:
			audio_interrupt_flag=none;
			setPassStatus(&curHisParam, quesCont, STATUS_PASS);
			quesCont++;
			audioPush(word_param.AudioInfo[0]);
			audioPush(AUDIO_PINGSUCCESS);
			audioPush(AUDIO_Encourage1+(rand()%3));
			audioPush(AUDIO_LETSREAD);
	        setSubWordPlayList();
			*psState=EXM_AUDIO_PLAY;
			exam_state_ret=EXM_MEDAL;
			staTimeContFlag=0;
            break;
            
        case EXM_MEDAL:
           if (quesCont>=ExamQuesMax)
           {
               quesCont = 0;
			   curHisParam.file.useTimes=tick/100;
			   playExamScore(curHisParam.file.useTimes,curHisParam.file.score);
			   switch(user_mode_last)
				{
					case MODE_EXAM:
						user_param.his_param.locExmHisID++;
						memcpy(&user_param.loc_Exm_His, &curHisParam, sizeof(EXAM_HIS_PARAM)); //将当前成绩备份记录
						break;
					case MODE_OL_EXAM:
						user_param.his_param.olExmHisID++;
						memcpy(&user_param.ol_Exm_His, &curHisParam, sizeof(EXAM_HIS_PARAM)); //将当前成绩备份记录
						break;
					case MODE_OL_BATTLE:
						user_param.his_param.olBattleHisID++;
						memcpy(&user_param.ol_battle_His, &curHisParam, sizeof(EXAM_HIS_PARAM)); //将当前成绩备份记录
						break;
				}
				continueFlag=1;
               	exam_state_ret = EXM_CONTINUE;
                *psState = EXM_AUDIO_PLAY;
           }
           else
           {
               //未满10个
               audioPush(AUDIO_PASS);
			   audioPush(audio_score(curHisParam.file.score));
			   audioPush(AUDIO_KEEPGOING);
			   *psState=EXM_AUDIO_PLAY;
				exam_state_ret=EXM_CHECK_USART_CLEAR;
           }
           break;
			
        case EXM_AUDIO_PLAY:
          if(mp3RecStatus==empty)
          {
            *psState=exam_state_ret;
          }
          break;
          
        default:
            *psState = EXM_STARTUP;
            break;
    }
}
