#define SongMax 20


typedef enum
{
    MUSIC_STARTUP = 0,
	MUSIC_BLE_STARTUP,
    MUSIC_PLAYSONG,
    MUSIC_PLAYLAST,
    MUSIC_PLAYNEXT,
    MUSIC_WAIT2S,
    MUSIC_AUDIO_PLAY,
    MUSIC_CHECK,
    MUSIC_PLAYSPE,
    MUSIC_PAUSE,
} MUSIC_MODE_STATE;

extern void MusicMode(MUSIC_MODE_STATE *psState);
void setSongIdx(uint8_t idx,uint8_t offset);
uint8_t getSongIdx(void);

