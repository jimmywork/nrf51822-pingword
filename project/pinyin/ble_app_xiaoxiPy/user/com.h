#include <stdint.h>

//蓝牙协议指令码
#define shakeHand       0x01
#define appRomWrite     0x02
#define readFwVerson    0x03
#define resetAndRun     0x04

#define setSeriNum    	0x05
#define getSeriNum      0x06

#define setVolume		0x07
#define stopResume		0x08

#define userInfoRead    0xA1
#define userInfoErase   0xA2
#define quesGet         0xA3
#define chkExmHistory   0xA4
#define chkFreeHistory  0xA5
#define	exmGetOnline    0xA6
#define exmOLHistory    0xA7
#define staCardResponds	0xA8
#define stoCardResponds	0xA9
#define categoryStatus	0xAA
#define chkMyWordHistory 0xAC
#define staOlExmResponds 0xAB
#define stoOlExmResponds 0xAD
#define dataFlashWrite  0xF1
#define dataFlashRead   0xF2
#define dataFlashVerify 0xF3
#define dataFlashVersion 0xF4
#define setMusicPlay	0xB1
#define stoMusicPlay    0xB2
#define currentModeSto 0xae//51自动反馈

#define unitExamx 0xc0
#define uintExamHisx 0xc1
#define staUintExamResx 0xc2
#define stoUintExamResx 0xc3

#define currentModeCheck 0xd0

extern bool staAutoNotify;
void bleCom(uint8_t * p_data, uint16_t length);
void ResPonse(uint8_t cmd, uint8_t *source, uint16_t len);
extern void cycleResponds(void);