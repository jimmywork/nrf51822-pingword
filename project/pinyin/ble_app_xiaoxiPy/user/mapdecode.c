#include "common.h"
//返回真则表示当前组合存在，否则不存在

extern const uint16_t audio_26node[26]={
1, 11, 92, 259, 369, 379, 427, 523, 620, 638, 709, 799, 926, 1020, 1138, 1144, 1230, 1301, 1369, 1535, 1631, 2023, 1645, 1689, 1761, 1814,
};

uint8_t getWordStr(void)
{
	char *sta,*sto,*tmp;
	uint8_t i;
	sta=input_param.charBuff;
	i=0;
	while(i++<CARDS_NUMBER)
	{
          if(*sta)
            break;
          sta++;
	}
        input_param.sta_pos=i-1;
	i=0;
	sto=input_param.charBuff+CARDS_NUMBER-1;
	while(i++<CARDS_NUMBER)
	{
		if(*sto)
		  break;
		sto--;
	}
        memset(input_param.word_str,0,sizeof(input_param.word_str));
        if(sta<=sto)
        {
          tmp=sta;
          while(tmp!=sto)
          {
                  if(*tmp==0)
                  {
                  *tmp='_';
                  }
                  tmp++;
          }
          memcpy(input_param.word_str,sta,sto-sta+1);
          input_param.word_str_len=strlen(input_param.word_str);
        }
        else
        {
            input_param.word_str_len=0;
        }
	input_param.last_Insert_Pos=input_param.Insert_Pos;
		
        //NRF_LOG_INFO("str: %s,len: %d\r\n",input_param.word_str, input_param.word_str_len);
        return input_param.word_str_len;
}

uint16_t getWordData(char *word, uint8_t wordLen)
{
    uint16_t index;
    const char *datatmp;
    uint8_t u8StrLen;
	NRF_LOG_DEBUG("getWordData:%d,<%s>\r\n",wordLen,(uint32_t)word);
    for (index = 0; index < AUDIO_PY_MAP_CODE_LEN; index++)
    {
        datatmp = AUDIO_PY_MAP_CODE[index];
        u8StrLen = 0;
        while (isPyCard(datatmp[0]))
        {
            u8StrLen++;
            datatmp++;
        }
        datatmp = AUDIO_PY_MAP_CODE[index];
        if(index==398)
          NRF_LOG_HEXDUMP_DEBUG(datatmp, u8StrLen);
        if (memcmp(word, datatmp, min(max(u8StrLen, wordLen), CARDS_NUMBER)) == 0)
        {
            return index;
        }
    }
    NRF_LOG_DEBUG("wordidx:%x\r\n",index);
    return 0xffff;
}

const uint8_t tFullWord[6] = {0X20, 0X30, 0X38, 0X3c, 0X3e, 0X3f};
uint8_t getCurrentPyLearn(uint16_t index)
{
    const char *source;
	uint8_t i, cont = 0, u8StrLen = 0, pos = 0;
	uint8_t type, toneNum, speNum = 0;
	uint8_t noteS = 0, noteY1 = 0, noteY2 = 0;
	uint8_t a, b;
     
	word_param.CurrentWordIndex = index;
    source = AUDIO_PY_MAP_CODE[index];
	memset(word_param.CurrentWord, 0, sizeof(word_param.CurrentWord));
	cont=0;
        
    while (isPyCard(source[cont])&&(cont<CARDS_NUMBER))
    {
        word_param.CurrentWord[u8StrLen++] = source[cont++];
    }
	word_param.CurrentWordLen=cont;
    if (!u8StrLen)
    {
        return 0;
    }
    
	word_param.FullWord = tFullWord[word_param.CurrentWordLen - 1];
    //拆分提取
    type = source[cont++];
    toneNum = type & 0x0f;
	toneNum=(toneNum<=4)?(toneNum):0;
    type = (type >> 4) & 0x0f;
    type=(type>=6)?(type-6):0;//避免与字母重叠
	word_param.nodeType=TYPE_WORD;
    switch (type)
    {
        case TYPE_A:
            speNum = 1;
            noteS = word_param.FullWord;
		    word_param.nodeType=TYPE_NODE;
            break;
        case TYPE_AB:
            speNum = 2;
            noteS = tFullWord[0];
            noteY1 = word_param.FullWord & (~noteS);
            break;
        case TYPE_ABC:
            speNum = 3;
            noteS = tFullWord[0];
            noteY1 = 0x10;
            noteY2 = word_param.FullWord & (~(noteS | noteY1));
            break;
        case TYPE_AAB:
            speNum = 2;
            noteS = tFullWord[1];
            noteY1 = word_param.FullWord & (~noteS);
            break;
        case TYPE_AABC:
            speNum = 3;
            noteS = tFullWord[1];
            noteY1 = 0x08;
            noteY2 = word_param.FullWord & (~(noteS | noteY1)) ;
            break;
        default:
            return 0;
            break;
    }
	//拆分灯信息
    word_param.SubWordRgb[pos++] = word_param.FullWord;
	#ifdef readDouble
	word_param.SubWordRgb[pos++] = word_param.FullWord;
	#endif
    for (i = 0; i < toneNum; i++)
    {
        word_param.SubWordRgb[pos++] = noteS;
		#ifdef readDouble
		word_param.SubWordRgb[pos++] = noteS;
		#endif
        if (speNum > 1)
        {
            word_param.SubWordRgb[pos++] = noteY1;
			#ifdef readDouble
			word_param.SubWordRgb[pos++] = noteY1;
			#endif
        }
        if (speNum > 2)
        {
            word_param.SubWordRgb[pos++] = noteY2;
			#ifdef readDouble
			word_param.SubWordRgb[pos++] = noteY2;
			#endif
        }
        if (speNum > 1)
        {
            word_param.SubWordRgb[pos++] = word_param.FullWord;
			#ifdef readDouble
			word_param.SubWordRgb[pos++] = word_param.FullWord;
			#endif
        }
    }
    if (!pos)
    {
        return 0;
    }
	NRF_LOG_INFO("audio cont:%d\r\n",pos);
    word_param.CurrentSubPartNum = pos;
    memset(word_param.AudioInfo, 0, sizeof(word_param.AudioInfo));
	#ifdef readDouble
	for (i = 0; i < word_param.CurrentSubPartNum; i+=2)
	#else
    for (i = 0; i < word_param.CurrentSubPartNum; i++)
	#endif
    {
        a = source[cont];
        b = source[cont + 1];
		cont += 2;
		word_param.AudioInfo[i] = BUILD_UINT16(b, a);
		#ifdef readDouble
		word_param.AudioInfo[i+1] = word_param.AudioInfo[i];
		#endif
        NRF_LOG_INFO("audio idx:%x\r\n",word_param.AudioInfo[i]);
    }
    NRF_LOG_INFO("decode fini!\r\n");
    return 1;
}

uint8_t setSubWordPlayList(void)
{
    uint8_t i;
	uint8_t tmp;
	if(word_param.CurrentSubPartNum)
	{
		for(i=0;i<word_param.CurrentSubPartNum;i++)
		{
			tmp=word_param.SubWordRgb[i]>>input_param.sta_pos;
			NRF_LOG_INFO("rgb b:%x f:%x\r\n",word_param.SubWordRgb[i],tmp);
			mp3_rgb_push(tmp,COL_GREEN);
			audioPush(word_param.AudioInfo[i]);
            mp3_rgb_push(RGB_CARD_SEL,NONE);
			mp3_Delay(10);
		}
		return true;
	}
	return false;
}


