#include "common.h"
 
#define SYS_PARAM_ID    1  //这个ID不重复即可
#define SYS_SN_KEY     1 //KEY，也能不重复，一个ID可以有多个KEY,1，2，3...

#define USER_PARAM_ID    2  //这个ID不重复即可
#define USER_SN_KEY     2 

static bool volatile m_fds_initialized=false;
	
static void fds_evt_handler(fds_evt_t const * p_evt)
{
    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == NRF_SUCCESS)
            {
                NRF_LOG_DEBUG("fds init success\r\n");
                m_fds_initialized = true;
            }
            break;

        case FDS_EVT_WRITE:
        {
            if (p_evt->result == NRF_SUCCESS)
            {
                NRF_LOG_DEBUG("Record ID:\t0x%04x",  p_evt->write.record_id);
                NRF_LOG_DEBUG("File ID:\t0x%04x",    p_evt->write.file_id);
                NRF_LOG_DEBUG("Record key:\t0x%04x", p_evt->write.record_key);
            }
            else
            {
                NRF_LOG_DEBUG("WRITE NRF CODE:%X",  p_evt->result);
            }
        } break;

//        case FDS_EVT_DEL_RECORD:
//        {
//            if (p_evt->result == NRF_SUCCESS)
//            {
//                NRF_LOG_INFO("Record ID:\t0x%04x",  p_evt->del.record_id);
//                NRF_LOG_INFO("File ID:\t0x%04x",    p_evt->del.file_id);
//                NRF_LOG_INFO("Record key:\t0x%04x", p_evt->del.record_key);
//            }
//            m_delete_all.pending = false;
//        } break;

        default:
            break;
    }
}

/**@brief   Wait for fds to initialize. */
static void wait_for_fds_ready(void)
{
    while (!m_fds_initialized);
}

//void factory_reset(){
//    ret_code_t err_code;
//
//    /* Register first to receive an event when initialization is complete. */
//    (void) fds_register(fds_evt_handler);
//
//    err_code = fds_init();
//    APP_ERROR_CHECK(err_code);
//
//    wait_for_fds_ready();
//
//    fds_stat_t stat = {0};
//
//    err_code = fds_stat(&stat);
//    APP_ERROR_CHECK(err_code);
//
//    fds_record_desc_t desc = {0};
//    fds_find_token_t tok   = {0};
//
//    if (fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &tok) == NRF_SUCCESS)
//    {
//        ret_code_t rc = fds_record_delete(&desc);
//        if (rc != NRF_SUCCESS)
//        {
//            NRF_LOG_INFO("FDS DELETE ERROR");
//        }else{
//            NRF_LOG_INFO("Reseted to factory");
//        }
//
//    }else{
//        NRF_LOG_INFO("Record doesnt exist");
//    }
//}


void flash_init(void)
{
#ifdef USER_INFO_FLASH
  ret_code_t err_code;
    if(!m_fds_initialized){
        /* Register first to receive an event when initialization is complete. */
        (void) fds_register(fds_evt_handler);
        err_code = fds_init();
        APP_ERROR_CHECK(err_code);
        wait_for_fds_ready();
	NRF_LOG_DEBUG("flash init:%d\r\n",err_code);
    }
#endif
}


void paramSave(uint8_t *source, uint16_t size,uint8_t diff)
{
#ifdef USER_INFO_FLASH
  ret_code_t err_code;
  uint32_t  buff[256];
  fds_find_token_t tok = {0};
  fds_record_desc_t dvr_sn_desc={0};
 
  fds_record_chunk_t chunk;
  fds_record_t record;
	
	
  memcpy((uint8_t *)buff, source, size);
  uint16_t u32Size=size/4;
  u32Size += (size%4)?1:0;
  
  if(diff==PAGE_USER)
  {
  	record.file_id = USER_PARAM_ID;
	record.key = USER_SN_KEY;
  }
  else
  {
  	record.file_id = SYS_PARAM_ID;
	record.key = SYS_SN_KEY;
  }

  chunk.p_data = buff;
  chunk.length_words = u32Size;
 
	record.data.p_chunks = &chunk;
	record.data.num_chunks = 1;
  NRF_LOG_DEBUG("flash size:%x\r\n",u32Size);      
  err_code=fds_record_find(record.file_id, record.key, &dvr_sn_desc, &tok);
  if(err_code == FDS_SUCCESS)
  {
      err_code=fds_record_update(&dvr_sn_desc, &record);
      NRF_LOG_DEBUG("flash update:%d\r\n",err_code);
  }
  else
  {
      err_code=fds_record_write(&dvr_sn_desc, &record);
      NRF_LOG_DEBUG("flash write:%d\r\n",err_code);
  }
  if(err_code==FDS_ERR_NO_SPACE_IN_FLASH)
  {
     fds_gc();
  }
#endif
}

ret_code_t paramLoad(uint8_t *source, uint16_t size,uint8_t diff)
{
#ifdef USER_INFO_FLASH  
	uint16_t file_id,key;
	fds_find_token_t tok = {0};
	fds_record_desc_t dvr_sn_desc = {0};
	uint32_t err_code;
	  if(diff==PAGE_USER)
	  {
	  	file_id = USER_PARAM_ID;
		key = USER_SN_KEY;
	  }
	  else
	  {
	  	file_id = SYS_PARAM_ID;
		key = SYS_SN_KEY;
	  }
	err_code=fds_record_find(file_id, key, &dvr_sn_desc, &tok);
	if(err_code == FDS_SUCCESS)
	{
		uint16_t i = 0;
		fds_flash_record_t record = {0};
		fds_record_open(&dvr_sn_desc, &record);
		uint8_t *p = (uint8_t*)record.p_data;
		for(i = 0; i < size; i++)
		{
         source[i] = *(p+i);
		}
		fds_record_close(&dvr_sn_desc);
        SEGGER_RTT_printf_BUFFER("fds read",source,size);
	}
	else
	{
          NRF_LOG_ERROR("FDS READ ERROR:%d\r\n",err_code);
		//fds_record_delete(&dvr_sn_desc);
	}
	return err_code;
#endif
}
