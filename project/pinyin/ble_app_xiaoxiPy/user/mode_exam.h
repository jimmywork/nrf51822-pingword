typedef enum
{
    EXM_STARTUP = 0,
    EXM_STARTUP2,
    EXM_CONTINUE,
    EXM_GET_CUR_WORD,
    EXM_NOACT,
    EXM_JUMOPVER,
    EXM_CHECK_USART_CLEAR,
    EXM_FIRSTCOLOR,
    EXM_CHECK_USART,
    EXM_WORD_CORRECT,
    EXM_MEDAL,
    EXM_AUDIO_PLAY,
} EXAM_MODE_STATE;

typedef enum
{
    LOCAL_EXAM = 0,
	OL_BATTLE,
    OL_EXAM,
} EXAM_RUN_MODE;


extern uint16_t exam_currentQues;

void ExamMode(EXAM_MODE_STATE *psState);
