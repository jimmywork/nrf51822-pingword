/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

/** @file
 * @defgroup nrf_dev_timer_example_main main.c
 * @{
 * @ingroup nrf_dev_timer_example
 * @brief Timer Example Application main file.
 *
 * This file contains the source code for a sample application using Timer0.
 *
 */
#include "common.h"
#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "nrf_drv_timer.h"
#include "bsp.h"
#include "app_error.h"
#include "app_fifo.h"

static void studyModeSelect(void);

//定时器设计使用
APP_TIMER_DEF(m_timer_id);
APP_TIMER_DEF(m_ui_timer_id);
#define TIME_USER_APP_INTERVAL	APP_TIMER_TICKS(10, APP_TIMER_PRESCALER)//10ms执行一次
#define TIME_USER_UI_INTERVAL	APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)//100ms执行一次
static bool checkUSB(void)
{
	static bool flag=false;
	if(isUSBInsert())
	{
		if(!flag)
		{
			flag=true;
			usbConMode=usb_idle;
			audio_force_Interrupt();
			mp3_CheckUsb();
		}
		else
		{
			if((mp3RecStatus==timeout)||(usbConMode==usb_driver)||(usbConMode==usb_error))
			{
				//SET_RGB(RGB_5_SEL, COL_RED);
				u8PowerOnFlag=POW_OFF;
				main_state=MAIN_STATE_POWER_OFF;
			}
			else if(usbConMode==usb_normal)//非USB连接模式
			{
				//SET_RGB(RGB_4_SEL, COL_GREEN);
				audioPush(AUDIO_CHARGE);
				audioPush(AUDIO_BYEBYE);
				power_off();
			}
			else
			{
				//SET_RGB(RGB_6_SEL, COL_YELLOW);
			}
		}
		return true;
	}
	return false;
}

static void sys_states_machine(void)
{
    static user_sys_state_t main_state_ret =MAIN_POWER_ON;
    static uint32_t u32Cont=0;
    if(u8PowerOnFlag)
    {
        nrf_gpio_pin_toggle(GPIO_WDT);//喂狗
    }
    if(u8PowerOnFlag==POW_BYEBYE)
    {
      return; 
    }
    switch(main_state)
    {
      case MAIN_POWER_CHECK:
		if(!u8PowerOnFlag)
        {
        	if(u32Cont++>150)//1.5秒
        	{
        		u32Cont=0;
	        	u8PowerOnFlag=POW_STANDBY;
				if(!checkBattary())
				{
					main_state=MAIN_POWER_ON;
				}
        	}
		else
		{
			if(!bsp_button_is_pressed(BSP_EVENT_KEY_POWER-BSP_EVENT_KEY_INS_1))
			{
				u32Cont=0;
			}
		}
        }
		break;
		
      case MAIN_POWER_ON:
        if(u8PowerOnFlag==POW_STANDBY)
        {
        //允许工作电压下，先亮蓝灯在亮红灯
          SET_RGB(RGB_POWER_SEL, COL_BLUE);	
          main_state=MAIN_STATE_MP3_CFG_STEP1;
        }
        break;
        
      case MAIN_STATE_MP3_CFG_STEP1:
        mp3_SetVolume(sys_param.Sys_Vol);
        main_state=MAIN_STATE_MP3_WAIT_RES;
        break;
		
      case MAIN_STATE_MP3_WAIT_RES:
		if(u8PowerOnFlag==POW_ON)
        {
          main_state=MAIN_STATE_WORK_MODE_SEL;
        }
		break;
		
      case MAIN_STATE_WORK_MODE_SEL:
        //<测试模式><正常工作><USB插入关机>
        if(bsp_button_is_pressed(BSP_EVENT_KEY_FUN_5-BSP_EVENT_KEY_INS_1))
        {
          main_state=MAIN_STATE_TEST_STARTUP;
        }
		else
		{
          main_state=MAIN_STATE_PO_CHK_USB;
        }	
        break;
		
	  case MAIN_STATE_PO_CHK_USB:
		if(!checkUSB())
		{
			main_state=MAIN_STATE_MAIN_STARTUP;
		}
	  	break;
      
      case MAIN_STATE_MAIN_STARTUP:
        audioPush(AUDIO_nWELCOME);
		audioPush(AUDIO_TOTAL);
		audio_play_star();
        NRF_LOG_INFO("MAIN_STATE_STARTUP\r\n");
        main_state_ret=MAIN_STATE_MAIN_LOOP;
        main_state=MAIN_STATE_MAIN_PLAY;
        break;
      
      case MAIN_STATE_TEST_STARTUP:
	  	eTestModeState=TEST_STARTUP;
		main_state=MAIN_STATE_TEST_LOOP;
        break;
        
      case MAIN_STATE_MAIN_LOOP:
	  	if(checkBattary()||checkUSB())
	  	{
	  		break;
	  	}
        studyModeSelect();
        break;
        
      case MAIN_STATE_TEST_LOOP:
        TestMode(&eTestModeState);
        break;
      
      case MAIN_STATE_MAIN_PLAY:
        if(mp3RecStatus==empty)
        {
          main_state=main_state_ret;
        }
        break;
		
      case MAIN_STATE_POWER_OFF:
	  	break;
		
      default:
      break;
    }
}

user_mode_t user_mode_last=MODE_FREE;
static void studyModeSelect(void)
{
  switch(user_mode_last)
  {
    case MODE_FREE:
      FreeMode(&eFreeModeState);
      break;
    case MODE_STUDY:
	case MODE_OL_MYWORD:
      StudyMode(&eStudyModeState);
      break;
    case MODE_EXAM:
	case MODE_OL_EXAM:
	case MODE_OL_BATTLE:
      ExamMode(&eExamModeState);
      break;
	case MODE_MUSIC:
      MusicMode(&eMusicModeState);
      break;
  }
}

static void app_standalone(void)
{
    checkCardInput();
    sys_states_machine();
    user_interface();
}

bool app_timeout=false;
static void TIME_timeout_handler(void *p_context)
{
	UNUSED_PARAMETER(p_context);
	app_timeout=true;
    app_standalone();
}


#define SEC_CONT 10
#define MIN_CONT 60

static void TIME_ui_timeout_handler(void *p_context)
{
    
	static uint8_t goto_sleep_flag=0;
	cycleResponds();
	info_record();
	RGB_BLINK();
	if(goto_sleep_flag&&(!noAct_1s_cont))
	{
		goto_sleep_flag=0;
		cancel_power_off();
	}
    noAct_100ms_cont++;
	if(noAct_100ms_cont%SEC_CONT==0)
	{
		if((user_mode_last<=MODE_OL_BATTLE)&&(user_mode_last>=MODE_EXAM)&&(eExamModeState!=EXM_CHECK_USART))
		{
			return ;
		}
		noAct_1s_cont++;
		test_pin();
		switch(noAct_1s_cont)
		{
			case 2:
			case 7:
			case 12:
			case 17:
			case 22:	
				if((user_mode_last<=MODE_OL_BATTLE)&&(user_mode_last>=MODE_EXAM)&&(eExamModeState==EXM_CHECK_USART))
				{
					playCurrentQues(exam_currentQues);
					eExamModeState=EXM_NOACT;
				}
				break; 
			
			case MIN_CONT*3:
				audioPush(AUDIO_SLEEP1);
				break;
			case MIN_CONT*6:
				audioPush(AUDIO_SLEEP2);
				break;
			case MIN_CONT*9:
				audioPush(AUDIO_SLEEP3);
				break;
			case MIN_CONT*10:
				audioPush(AUDIO_SLEEP);
				power_off();
				goto_sleep_flag=1;
				break;
		}
	}
}
/**
 * @brief Function for main application entry.
 */
void user_app_init(void)
{
    uint32_t err_code;
    
    //创建一个定时，设置定时器模式
    err_code = app_timer_create(&m_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                TIME_timeout_handler);
    err_code = app_timer_create(&m_ui_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                TIME_ui_timeout_handler);
    
    
    APP_ERROR_CHECK(err_code);
}

void user_app_start(void)
{
    uint32_t err_code;//Start application timers.定时时间间隔
    
    err_code = app_timer_start(m_timer_id, TIME_USER_APP_INTERVAL, NULL);
    err_code = app_timer_start(m_ui_timer_id, TIME_USER_UI_INTERVAL, NULL);
    APP_ERROR_CHECK(err_code);
}
/** @} */
