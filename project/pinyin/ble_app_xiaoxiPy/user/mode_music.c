#include "common.h"
static uint8_t song_Idx = 0;
static uint8_t song_left=SongMax;

uint8_t getSongIdx(void)
{
	return song_Idx;
}

void setSongIdx(uint8_t idx,uint8_t offset)
{
	if(offset)
	{
		song_Idx++;
	}
	else
	{
		song_Idx=idx;
	}
	song_left=SongMax;
}

void MusicMode(MUSIC_MODE_STATE *psState)
{
    static MUSIC_MODE_STATE music_state_ret=MUSIC_STARTUP;
	static uint16_t tick=0;
    switch (*psState)
    {
        case MUSIC_STARTUP:
			song_Idx=0;
		case MUSIC_BLE_STARTUP:
            NRF_LOG_INFO("music mode\r\n");
			SET_RGB(RGB_CARD_SEL, NONE);
	    	audio_interrupt_flag=key_interrupt;
            audioPush(AUDIO_MUSIC_MODE);
            *psState=MUSIC_AUDIO_PLAY;
            music_state_ret=MUSIC_PLAYSONG;
            break;

		case MUSIC_PLAYSONG:
			audioPush(audio_song_sel(song_Idx));
			music_state_ret = MUSIC_WAIT2S;
            *psState = MUSIC_AUDIO_PLAY;
			tick=0;
			break;
		
        case MUSIC_WAIT2S:
	    	if(!tick)
			{
				if(isBleConnected())
					ResPonse(stoMusicPlay, &song_Idx, 1);
			}
			if(tick++>200)
			{
				tick=0;
				*psState = MUSIC_PLAYNEXT;
			}
            break;
			
        case MUSIC_PLAYNEXT:
			if(song_left--==0)
			{
				*psState = MUSIC_PAUSE;
                //return true;//关机
			}
            song_Idx++;
			if (song_Idx >= SongMax)
            {
                song_Idx = 0;
            }
			*psState = MUSIC_PLAYSONG;
            break;  

		case MUSIC_PAUSE:
			break;

        case MUSIC_AUDIO_PLAY:
          if(mp3RecStatus==empty)
          {
            *psState=music_state_ret;
          }
          break;
          
        default:
            *psState = MUSIC_STARTUP;
            break;
    }
}