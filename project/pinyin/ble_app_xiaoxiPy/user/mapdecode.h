typedef enum
{
    TYPE_A,
    TYPE_AB,
    TYPE_AAB,
    TYPE_ABC,
    TYPE_AABC,
} PY_SPE_TYPE;

uint8_t getWordStr(void);
uint16_t getWordData(char *word, uint8_t wordLen);
uint8_t getCurrentPyLearn(uint16_t index);
uint8_t setSubWordPlayList(void);

extern const uint8_t tFullWord[6];