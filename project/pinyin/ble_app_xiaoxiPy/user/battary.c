#include "nrf.h"
#include "common.h"
#include <stdint.h>
#include <string.h>
#include "boards.h"
#include "nrf_drv_adc.h"
/* adc */
#define ADC_BUFFER_SIZE 10                                /**< Size of buffer for ADC samples.  */
static nrf_drv_adc_channel_t m_channel_config = NRF_DRV_ADC_DEFAULT_CHANNEL(NRF_ADC_CONFIG_INPUT_6); /**< Channel instance. Default configuration used. */
static nrf_adc_value_t adc_buffer[ADC_BUFFER_SIZE]; /**< ADC buffer. */

/* 计算电压平均值 */
static void GetAVG(int16_t *adcBuff,uint8_t size)
{
    uint32_t aver = 0;
    uint16_t maxvalue, minvalue;
    uint16_t AdcTmp;
    uint8_t i;
    
    if(size!=10)
    {
       return ;
    }
    
    aver = maxvalue = minvalue = adcBuff[0];
    for (i = 1; i < size; i++)
    {
        if (maxvalue < adcBuff[i])
            maxvalue = adcBuff[i];
        if (minvalue > adcBuff[i])
            minvalue = adcBuff[i];
        aver += adcBuff[i];
    }
    aver -= maxvalue + minvalue;
    AdcTmp = aver >> 3;//除以8 

    if (AdcTmp <= 0)
    {
        CurrentVbat = 0;
    }
    else if (AdcTmp >= 1024)
    {
        CurrentVbat = 4200;
    }
    else
    {
        CurrentVbat = (uint16_t)((AdcTmp * 3.3 / 1024.0) * 44.0 / 33.0 * 1000);
    }
}

/**
 * @brief ADC interrupt handler.
 */
static void adc_event_handler(nrf_drv_adc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_ADC_EVT_DONE)
    {
        GetAVG(p_event->data.done.p_buffer,(uint8_t)p_event->data.done.size);
    }
}

void adc_init(void)
{
    ret_code_t ret_code;
    nrf_drv_adc_config_t config = NRF_DRV_ADC_DEFAULT_CONFIG;

    ret_code = nrf_drv_adc_init(&config, adc_event_handler);
    APP_ERROR_CHECK(ret_code);

    nrf_drv_adc_channel_enable(&m_channel_config);
}

void adc_sample_times(void)
{
	static uint8_t sampleCont=0;
	  if(sampleCont++>=ADC_BUFFER_SIZE-1)
	  {
	     sampleCont=0;
	     APP_ERROR_CHECK(nrf_drv_adc_buffer_convert(adc_buffer,ADC_BUFFER_SIZE));//每10次触发中断
	  }
	  nrf_drv_adc_sample();	
}

static uint8_t BattaryLevel = 0;
bool checkBattary(void)
{
  
  if (CurrentVbat)
    {
        if (CurrentVbat <= LOW_POWER_Off) //0%-关机
        {
            if (BattaryLevel < 5)
            {
            	BattaryLevel = 5;
				SET_RGB(RGB_ALL_SEL, NONE);
		        if(u8PowerOnFlag==POW_ON)//正常工作后没电需要先播报语音再关机
                {
                    audio_force_Interrupt();
			        audioPush(AUDIO_NOPOWER);
					power_off();
					StopRgbBlink(RGB_POWER_SEL);
                }
                else//没正常工作的话亮红灯后直接关机
                {
                    StopRgbBlink(RGB_POWER_SEL);
                    u8PowerOnFlag=POW_OFF;
					main_state=MAIN_STATE_POWER_OFF;
                }
                SET_RGB(RGB_POWER_SEL, PowerLowCol);
				return true;
            }
        }
        else if (CurrentVbat <= LOW_POWER_SuperLow) //<10%
        {
            if(u8PowerOnFlag!=POW_ON)
            {
                return false;
            }
            if (BattaryLevel < 4)
            {
            	BattaryLevel = 4;
				audioPush(AUDIO_SUPERLOWPOWER);
                SetRgbBlink(PowerLowCol, RGB_POWER_SEL, 0xff, BlinkSlow);
            }
        }
        else if (CurrentVbat <= LOW_POWER_Low) //<20%
        {
			if(u8PowerOnFlag!=POW_ON)
            {
                return false;
            }
            if (BattaryLevel < 3)
            {
            	BattaryLevel = 3;
				audioPush(AUDIO_LOWPOWER);
				SET_RGB(RGB_POWER_SEL, PowerLowCol);
				StopRgbBlink(RGB_POWER_SEL);
            }
        }
        else//电量充足
        {
	    if(u8PowerOnFlag!=POW_ON)
            {
                return false;
            }
            if (BattaryLevel < 1)
            {
            	BattaryLevel = 1;
				SET_RGB(RGB_POWER_SEL, PowerGoodCol);
				StopRgbBlink(RGB_POWER_SEL);
            }
        }
        CurrentVbat = 0;
    }
  	return false;
}