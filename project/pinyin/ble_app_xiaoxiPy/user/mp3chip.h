typedef enum
{
    WT_StopPlaying=0xab,
    WT_PlaySpecific=0xa5,
    WT_SetVol=0xae,
    WT_CheckDevStatus=0xca,
} wt2003s_cmd_t;

typedef enum
{
    code_mp3=0,
    code_cmd,
    code_rgb,
    code_delay,
} fifo_code_type_t;

typedef enum
{
    cmd_setvol=0,
    cmd_stop,
    cmd_delay,
    cmd_chkusb,
} mp3_cmd_type_t;

typedef enum
{
	none,
    insert_interrupt=0x01,
	key_interrupt=0x02,	
	all_interrupt=0x03,
	force_interrupt=0xff,
} mp3_interrupt_type_t;

typedef enum
{
    idle,
    standby,
    busy,
    play_sta,
    cmd_sta,
    cmd_sto,
    delay_sta,
    ok,
    fail,
    timeout,
    empty,
} mp3_rec_state_t;

void uart_init(void);
uint8_t mp3_Play(uint16_t index);
void mp3_Stop(void);
void mp3_CheckUsb(void);
void user_interface(void);
void mp3FifoInit(void);
void fifoTest(void);
void audioPush(uint16_t code);
void mp3_SetVolume(uint8_t u8Volume);
uint8_t is_aduio_empty(void);
void audio_Interrupt_Reset(uint8_t inter_type);
void mp3_Delay(uint8_t u100ms);
uint8_t isVolAudio(uint16_t idx);
void audio_stop_clear(void);
void audio_force_Interrupt(void);
void AUDIO_CMD(uint8_t cmd,uint8_t * pack,uint8_t len,uint32_t timeOut);
	
extern uint16_t mp3_pre_play;
extern mp3_rec_state_t mp3RecStatus;
extern uint8_t audio_interrupt_flag;
extern uint8_t force_interrupt_flag;