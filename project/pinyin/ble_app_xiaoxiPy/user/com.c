 #include "common.h"

static void dataDecode(uint8_t input);
static void response_event_handle(uint8_t input,uint8_t sto);
static void cmdProcess(uint8_t *source,uint16_t len);

#define bleRecLenMax 100

typedef enum
{
    CMD_STATE_HEAD,
    CMD_STATE_GET_INDEX,
    CMD_STATE_GET_PACK_LEN_H,
    CMD_STATE_GET_PACK_LEN_L,
    CMD_STATE_GET_DATA,
    CMD_STATE_GHECK_CRC,
    CMD_STATE_RESPONSE,
} xiao_cmd_decode_state_t;

static uint8_t PackageIndex=0;
bool staAutoNotify=false;

void bleCom(uint8_t * p_data, uint16_t length)
{
  uint16_t i=0;
  //开始定时器，判断接收超时
  
  while(i<length)
  {
    dataDecode(p_data[i++]);
  }
}

static void dataDecode(uint8_t input)
{
  static uint8_t Package[bleRecLenMax];
  static xiao_cmd_decode_state_t decodeStatus=CMD_STATE_HEAD;
  static uint16_t recLen;
  static uint16_t offset=0;
  static uint8_t crc=0;
  static uint8_t PackageIndexTmp;
//  NRF_LOG_INFO("\r\ndataDecode status: %d input :%x  crc : %x\r\n",decodeStatus,input,crc);
  switch(decodeStatus)
  {
  case CMD_STATE_HEAD:
    if(input==0xaa)
    {
      decodeStatus=CMD_STATE_GET_INDEX;
    }
    break;
    
  case CMD_STATE_GET_INDEX:
    crc=0xaa;
    decodeStatus=CMD_STATE_GET_PACK_LEN_H;
    if (input >= PackageIndex)
    {
        PackageIndexTmp = input;
    }
    else
    {
        if (PackageIndex == 0xff)
        {
            PackageIndexTmp = input;
        }
        else
        {
            decodeStatus=CMD_STATE_HEAD;
        }
    }
    break;
    
  case CMD_STATE_GET_PACK_LEN_H:
    recLen=input;
    recLen<<=8;
    decodeStatus=CMD_STATE_GET_PACK_LEN_L;
    break;
    
  case CMD_STATE_GET_PACK_LEN_L:
    recLen|=input;
    if(recLen>bleRecLenMax)
    {
       recLen=0;
       decodeStatus=CMD_STATE_HEAD;
    }
    else
    {
        offset=0;
        decodeStatus=CMD_STATE_GET_DATA;
    }
    break;  
    
  case CMD_STATE_GET_DATA:
    Package[offset++]=input;
    if((offset+4)>=recLen)
    {
      decodeStatus=CMD_STATE_GHECK_CRC;
    }
    break;
    
  case CMD_STATE_GHECK_CRC:
    if(crc==input)
    {
      PackageIndex=PackageIndexTmp;
      cmdProcess(Package,offset);
    }
    crc=0;
    offset=0;
    recLen=0;
    memset(Package,0,sizeof(Package));
    decodeStatus=CMD_STATE_HEAD;
    break;
    
  default:
    decodeStatus=CMD_STATE_HEAD;
    break;
  }
  crc^=input;
  //NRF_LOG_INFO("\r\ncrc after: %x\r\n",crc);
}

/* last byte should set "sto" as true ,otherwist set as false */
static void response_event_handle(uint8_t input,uint8_t sto)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t err_code;

    data_array[index++]=input;

    if ((index >= BLE_NUS_MAX_DATA_LEN)||(sto))
    {
        SEGGER_RTT_printf_BUFFER("ble_nus_string_send",data_array, index);
        err_code = ble_nus_string_send(&m_nus, data_array, index);
        if (err_code != NRF_ERROR_INVALID_STATE)
        {
            APP_ERROR_CHECK(err_code);
        }
        index = 0;
    }
}

void ResPonse(uint8_t cmd, uint8_t *source, uint16_t len)
{
    uint8_t cmdReverse, Response[5] = {0x55};
    uint16_t i, cmdlen;
    uint8_t crc;
    
    cmdReverse = cmd ^ 0xff;
    cmdlen = len + 5;
    Response[1] = PackageIndex;
    Response[2] = cmdlen >> 8;
    Response[3] = cmdlen;
    Response[4] = cmdReverse;
    crc = 0;
    for (i = 0; i < 5; i++)
    {
        response_event_handle(Response[i],false);
        crc ^= Response[i];
    }
    for (i = 0; i < len; i++)
    {
        response_event_handle(source[i],false);
        crc ^= source[i];
    }
    response_event_handle(crc,true);
}

static void FreeHisResPonse(void)
{
    uint8_t cmdReverse, Response[5] = {0x55};
    uint16_t i, cmdlen,val;
    uint8_t crc,valH,valL;
	uint32_t rtmp,wtmp;
    rtmp=user_param.freeHisFifo.read_pos;
	wtmp=user_param.freeHisFifo.write_pos;
    cmdReverse = chkFreeHistory ^ 0xff;
	cmdlen = recordMax-1;
	cmdlen<<=1;
    cmdlen += 5;
    Response[1] = PackageIndex;
    Response[2] = cmdlen >> 8;
    Response[3] = cmdlen;
    Response[4] = cmdReverse;
    crc = 0;
    for (i = 0; i < 5; i++)
    {
        response_event_handle(Response[i],false);
        crc ^= Response[i];
    }
    for (i = 0; i < recordMax; i++)
    {
    	if(app_int_fifo_get(&user_param.freeHisFifo, &val))
    	{
    		valH=val>>8;
    		response_event_handle(valH,false);
			crc ^= valH;
			valL=val;
			response_event_handle(valL,false);
			crc ^= valL;
    	}
		else
		{
			response_event_handle(0,false);
			response_event_handle(0,false);
		}
    }
    response_event_handle(crc,true);
	user_param.freeHisFifo.read_pos=rtmp;
	user_param.freeHisFifo.write_pos=wtmp;
}

static uint8_t ispCrc(uint8_t *source, uint16_t len)
{
    uint16_t i;
    uint8_t crc = 0;
    for (i = 0; i < len; i++)
    {
        crc ^= source[i];
    }
    return crc;
}


void SEGGER_RTT_printf_BUFFER(const char *tag,uint8_t *source,uint16_t len)
{
  NRF_LOG_INFO("%s--%d\r\n",(uint32_t)tag,len);
  NRF_LOG_HEXDUMP_INFO(source,len);
}

static void cmdProcess(uint8_t *source,uint16_t len)
{
  uint8_t cmd=source[0];
  uint8_t i,rData[100],packageSta,packageLen;
  uint16_t offset;
  EXAM_HIS_PARAM *tmp;
  uint32_t id;
  SEGGER_RTT_printf_BUFFER("ble cmd",source,len);
  packageSta=1;
  packageLen=len-1;
  switch(cmd)
  {
    case shakeHand:
	    rData[0] = 0x02;
	    ResPonse(cmd, rData, 1);
	    break;
		
    case userInfoRead://获取用户信息，例如小红花
	    rData[0] = user_param.Loc_Star >> 8;
	    rData[1] = user_param.Loc_Star;
	    ResPonse(cmd, rData, 2);
	    break;
		
	case userInfoErase://清除用户信息
        memset(&user_param,0,sizeof(USER_PARAM));
        rData[0] = 0;
        ResPonse(cmd, rData, 1);
        break;
		
	case chkFreeHistory:
		FreeHisResPonse();
		break;
		
    case quesGet:
		user_param.his_param.olStdHisID++;
		user_param.Ble_Study_Index=0;
		for (i = 0, offset = 0; i < packageLen / 2; i++, offset += 2)
        {
            user_param.Ble_Study[i] = BUILD_UINT16(source[packageSta + 1 + offset], source[packageSta + offset]);
        }
        user_param.Ble_Study_Num = packageLen / 2;
		setModify(RECORD_USER_PARAM);
		offset = 0;
		rData[offset++]	= user_param.his_param.olStdHisID>>24;
		rData[offset++]	= user_param.his_param.olStdHisID>>16;
		rData[offset++]	= user_param.his_param.olStdHisID>>8;
		rData[offset++]	= user_param.his_param.olStdHisID;
        ResPonse(cmd, rData, offset);

		audio_force_Interrupt();
		SET_RGB(RGB_CARD_SEL, NONE);
		user_mode_last=MODE_OL_MYWORD;
		eStudyModeState=ELMS_STARTUP;
    	break;
		
	case exmGetOnline:	
        for (i = 0, offset = 1; i < 10; i++, offset += 2)
        {
            user_param.Ble_Exam[i] = BUILD_UINT16(source[packageSta + 1 + offset], source[packageSta + offset]);
        }
        setModify(RECORD_USER_PARAM);
        rData[0] = ispCrc(source + packageSta, packageLen);
        ResPonse(cmd, rData, 1);
		switch(source[packageSta])
		{
			case OL_BATTLE:
			case OL_EXAM:
				audio_force_Interrupt();
				SET_RGB(RGB_CARD_SEL, NONE);
				user_mode_last=(user_mode_t)(MODE_EXAM+source[packageSta]);
				eExamModeState=EXM_STARTUP;
				break;
			default:
				break;
		}
        break;

	case chkExmHistory:
			tmp=&user_param.loc_Exm_His;
			id=user_param.his_param.locExmHisID;
			goto answer_his;
	case exmOLHistory:
			if(source[packageSta]==OL_BATTLE)
			{
				tmp=&user_param.ol_battle_His;
				id=user_param.his_param.olBattleHisID;
			}
			else if(source[packageSta]==OL_EXAM)
			{
				tmp=&user_param.ol_Exm_His;
				id=user_param.his_param.olExmHisID;
			}
			answer_his:
            offset = 0;
			rData[offset++]	= id>>24;
			rData[offset++]	= id>>16;
			rData[offset++]	= id>>8;
			rData[offset++]	= id;
            for (i = 0; i < 10; i++)
            {
                rData[offset++]	= tmp->file.quesLib[i] >> 8;
                rData[offset++]	= tmp->file.quesLib[i];
                rData[offset++]	= getPassStatus(tmp,i);
            }
			rData[offset++]	= tmp->file.useTimes >> 8;
	        rData[offset++]	= tmp->file.useTimes;
            ResPonse(cmd, rData, offset);
            break;
			
	case staCardResponds:	
            staAutoNotify = true;
            break;
	
    case stoCardResponds:
            staAutoNotify = false;
            rData[0] = 0;
            ResPonse(cmd, rData, 1);
            break;

	case currentModeCheck:
			offset = 0;
			rData[offset++]	= user_mode_last;
			switch(user_mode_last)
			{
				case MODE_MUSIC:
					rData[offset++]	= (eMusicModeState == MUSIC_PAUSE)?0:1;
					rData[offset++]	= getSongIdx();
					break;
				
				default:
					rData[offset++]	= 0xFF;
					break;
			}
			ResPonse(cmd, rData, offset);
			break;

	case setVolume:
			rData[0] = 0;
            ResPonse(cmd, rData, 1);
			setVolUM(source[packageSta]);
			break;
			
	case stopResume:
			rData[0] = 2;
			if(user_mode_last == MODE_MUSIC)
			{
				if((eMusicModeState == MUSIC_AUDIO_PLAY)||(eMusicModeState == MUSIC_PAUSE))
				{
					rData[0] = 0;
					if(eMusicModeState == MUSIC_PAUSE)
					{
						eMusicModeState = MUSIC_PLAYSONG;
					}
					else
					{
						audio_force_Interrupt();
						eMusicModeState = MUSIC_PAUSE;
					}
				}
			}
            ResPonse(cmd, rData, 1);
			break;
			
	default:
            rData[0] = 2;
            ResPonse(cmd, rData, 1);
            break;
  }
}

static void colTrans(uint8_t *output)
{
    uint8_t i, pos;
//    uint32_t *tmp;
    for (i = 0; i < CARDS_NUMBER; i++)
    {
#ifdef FORWARD
        pos = i;
#else
        pos = CARDS_NUMBER - 1 - i;
#endif
        output[pos] = 0;
//        tmp=rgbStrip.leds[i];
//        tmp[0]&=0xffffff;
//        switch (tmp[0])
//        {
//            case COL_LIB[COL_RED]:
//                output[pos] = 1;
//                break;
//            case COL_LIB[COL_GREEN]:
//                output[pos] = 2;
//                break;
//            case COL_LIB[COL_BLUE]:
//                output[pos] = 3;
//                break;
//            case COL_LIB[COL_PURPLE]:
//                output[pos] = 8;
//                break;
//            default:
//                break;
//        }
    }
}


void cycleResponds(void)
{		
	uint8_t Response[200];
	uint16_t i;
	uint8_t offset; 	
	uint16_t *lib;
	if(!(isBleConnected()&&staAutoNotify))
	{
		return ;
	}
	Response[0]=user_mode_last;
	memcpy(Response+1, input_param.charBuff, CARDS_NUMBER);
	colTrans(Response + 7);
	offset=13;
	switch(user_mode_last)
	{
	    
    	case MODE_STUDY:
			Response[offset++]	= user_param.Loc_Study_Index;
			break;
		
		case MODE_OL_MYWORD:
			Response[offset++]	= user_param.Ble_Study_Index;
			Response[offset++]	= user_param.Ble_Study_Num;
	        for (i = 0; i < user_param.Ble_Study_Num; i++)
	        {
	        	Response[offset++]	= user_param.Ble_Study[i] >> 8;
	            Response[offset++]	= user_param.Ble_Study[i];
	        }
			break;
			
	    case MODE_FREE:
			break;

		case MODE_EXAM:
		case MODE_OL_EXAM:
		case MODE_OL_BATTLE:
			switch(user_mode_last)
			{
				case MODE_EXAM:
					lib=user_param.loc_Exm_His.file.quesLib;
					break;
				
				case MODE_OL_EXAM:
				case MODE_OL_BATTLE:
					lib=user_param.Ble_Exam;
					break;
			}
			for (i = 0; i < ExamQuesMax; i++)
			{
			    Response[offset++] = lib[i]>>8;
				Response[offset++] = lib[i];
			}
			for (i = 0; i < ExamQuesMax; i++)
			{
			    Response[offset++] = getPassStatus(&curHisParam, i);;
			}
			break;	

		case MODE_MUSIC:
			staAutoNotify=false;
			offset=1;
			break;
			
		default:
			staAutoNotify=false;
			Response[0]=0xff;
			offset=1;
			break;
	}
	ResPonse(staCardResponds, Response, offset);
}

