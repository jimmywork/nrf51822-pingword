#include <stdint.h>
#include <string.h>
#include "ble_nus.h"
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"
#include "app_timer.h"
#include "fstorage.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "fds.h"

#include "user.h"
#include "app.h"
#include "com.h"
#include "mp3chip.h"
#include "neopixel.h"
#include "flash.h"
#include "battary.h"
#include "audiocode.h"
#include "cardread.h"
#include "mapdecode.h"
#include "mode_free.h"
#include "mode_exam.h"
#include "mode_study.h"
#include "mode_music.h"
#include "mode_test.h"
#include "learnCommon.h"
#include "ble_dfu.h"

#include "app_int_fifo.h"

#define USER_INFO_FLASH
#define BUILD_UINT16(loByte, hiByte) ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))
#define isPyCard(card) ((uint8_t)((card >= 'A') && (card <= 'Z')))
#define min(a,b)                ((a)>(b)?(b):(a))
#define max(a,b)                ((a)>(b)?(a):(b))

//系统参数设置
#define APP_TIMER_PRESCALER             0                                           /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         5                                           /**< Size of timer operation queues. */

#define volStep 3
#define VolumeMin 2
#define VolumeMid VolumeMin+2*volStep
#define VolumeMax VolumeMin+4*volStep

#define AUDIO_PY_MAP_CODE_LEN 448
//开机电源状态
typedef enum
{
    POW_OFF=0,
    POW_STANDBY,
    POW_ON,
    POW_BYEBYE,
} power_state_t;

//用户参数记录
#define BleQuesMax 30

typedef struct
{
  uint8_t Sys_FirstOn;  //第一次开机标志
  uint8_t Sys_Vol;      //音量
}
SYS_PARAM;

typedef struct
{
  uint32_t locExmHisID;
  uint32_t olExmHisID;
  uint32_t olStdHisID;
  uint32_t olBattleHisID;
}
HISTORY_PARAM;

#define recordMax 64//须为2的倍数
typedef struct
{
    uint16_t Loc_Star;          //小红花数量
    uint16_t Loc_Study_Index;    //本地学习进度

	app_int_fifo_t freeHisFifo;//学习历史
	uint16_t freeHisBuff[recordMax];

	HISTORY_PARAM his_param;
		
    uint8_t Ble_Study_Num;      //在线学习拼音数量
    uint16_t Ble_Study[BleQuesMax];
    uint16_t Ble_Study_Index;    //在线学习进度
    
    uint16_t Ble_Exam[ExamQuesMax];      //在线考试题目

	EXAM_HIS_PARAM loc_Exm_His;
    EXAM_HIS_PARAM ol_Exm_His;
	EXAM_HIS_PARAM ol_battle_His;
}USER_PARAM;

typedef enum
{
    TYPE_NODE,
	TYPE_WORD,
} node_type_t;

typedef enum
{
	MAIN_POWER_CHECK,
	MAIN_POWER_ON,
    MAIN_STATE_MP3_CFG_STEP1,
    MAIN_STATE_WORK_MODE_SEL,
    MAIN_STATE_PO_CHK_USB,
    MAIN_STATE_MAIN_STARTUP,
    MAIN_STATE_MAIN_LOOP,
    MAIN_STATE_TEST_STARTUP,
    MAIN_STATE_TEST_LOOP,
    MAIN_STATE_MAIN_PLAY,
    MAIN_STATE_MP3_WAIT_RES,
    MAIN_STATE_POWER_OFF,
} user_sys_state_t;
	
#define SubMax 60
typedef struct
{
    uint16_t CurrentWordIndex;
    uint8_t CurrentWordLen;
    char CurrentWord[CARDS_NUMBER];
	node_type_t nodeType;
	uint8_t FullWord;
	uint16_t SubWordRgb[SubMax];
	uint16_t AudioInfo[SubMax + 2];
    uint8_t CurrentSubPartNum;
    //uint8_t FullWord;
} SUBWORD_PARAM;

typedef enum
{
	usb_idle,
	usb_chking,
	usb_driver,
	usb_normal,
	usb_error,
} usb_state_t;
	
void helpFun(void);

extern FREE_MODE_STATE eFreeModeState;
extern STUDY_MODE_STATE eStudyModeState;
extern EXAM_MODE_STATE eExamModeState;
extern MUSIC_MODE_STATE eMusicModeState;
extern TEST_MODE_STATE eTestModeState;

extern user_sys_state_t main_state;
extern uint16_t CurrentVbat;
extern uint8_t u8PowerOnFlag;
extern USER_PARAM user_param;
extern SYS_PARAM sys_param;
extern SUBWORD_PARAM word_param;
//extern ePstorageState_t egPstorageState;
extern ble_nus_t m_nus;
extern ble_dfu_t m_dfus;
extern uint16_t m_conn_handle;
extern const char *AUDIO_PY_MAP_CODE[];
extern usb_state_t usbConMode;
extern void mp3_rgb_push(uint8_t sel,color_lib_t col);
extern void power_manage(void);
extern void SEGGER_RTT_printf_BUFFER(const char *tag,uint8_t *source,uint16_t len);
extern bool isBleConnected(void);