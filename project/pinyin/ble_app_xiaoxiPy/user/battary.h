#define LOW_POWER_Off           3360 //0%
#define LOW_POWER_SuperLow      3400 //5%
#define LOW_POWER_Low          	3496 //20%
#define LOW_POWER_Half          3600 //50%


#define PowerGoodCol 		COL_BLUE//电源正常
#define PowerLowCol 		COL_RED//低电

void adc_sample_times(void);
bool checkBattary(void);
void adc_init(void);
