typedef enum
{
    MODE_FREE,
    MODE_STUDY,
    MODE_OL_MYWORD,
    MODE_EXAM,
    MODE_OL_EXAM,
    MODE_OL_BATTLE,
    MODE_MUSIC,
    MODE_MAX
} user_mode_t;


void user_app_init(void);
void user_app_start(void);

extern user_mode_t user_mode_last;
extern bool app_timeout;