typedef enum
{
    UnKnown,
    InsertAndAllCorrect = 1,
    RemoveAndAllCorrect,
    PartialCorrect,
    RemoveError,
    InCorrect,
    MoveForward,
    InsertFirst,
    RemoveFrontError,
    NoSound,
} INSERT_RESULT;

typedef enum
{
	RECORD_USER_PARAM=0X01,
    RECORD_SYS_PARAM=0X02,
    RECODE_ALL=0x03
} RECORD_FLAG;


typedef enum
{
	STATUS_INIT=0,
    STATUS_PASS,
	STATUS_JUMP,
} PASS_STATUS;


typedef struct
{
	uint8_t quesSize;
	uint8_t *examPassStatus;
	uint16_t *quesLib;
    uint32_t useTimes;
	uint8_t score;
} his_source_param;	

#define ExamQuesMax 10
#define size2passSatatuSize(i) (i>>2+1)

typedef struct
{
	his_source_param file;
	uint8_t StatusBuff[size2passSatatuSize(ExamQuesMax)];
	uint16_t quesBuff[ExamQuesMax];
} EXAM_HIS_PARAM;	




#define InsertErrorCol 		COL_RED//错误提示
#define InsertCorrectCol 	COL_GREEN//正确提示

#define idx2rgb(i) (uint8_t)(i==RGB_POWER)?RGB_POWER_SEL:(0x20>>i)

#define idx2rgbN(i) (uint8_t)(i==RGB_POWER)?RGB_POWER_SEL:(1<<i)

INSERT_RESULT UpdateRgbReturn(bool nolight,bool chkRemove);
void audio_push_number(uint16_t num);
void tipNextChar(uint8_t specific);
void audioPush_NodeType(uint16_t index);

void passStatusInit(EXAM_HIS_PARAM *in,uint8_t size);

void setPassStatus(EXAM_HIS_PARAM *in,uint8_t idx,PASS_STATUS pass);
PASS_STATUS getPassStatus(EXAM_HIS_PARAM *in,uint8_t idx);
void playExamScore(uint32_t tick,uint8_t score);
void playCurrentQues(uint16_t currentQues);
void power_off(void);	
void cancel_power_off(void);
void info_record();
void setModify(uint8_t a);
void record_reinit(void);
void reset_record(void);
void audio_play_star(void);
uint8_t isSpecialPY(char *pStr);


extern uint32_t noAct_1s_cont;
extern uint32_t noAct_100ms_cont;
extern EXAM_HIS_PARAM curHisParam;
