#include "common.h"
uint16_t CurrentVbat=0;
uint8_t u8PowerOnFlag=POW_OFF;
usb_state_t usbConMode=usb_idle;
USER_PARAM user_param;
SYS_PARAM sys_param;
SUBWORD_PARAM word_param;

FREE_MODE_STATE eFreeModeState=FMS_STARTUP;
STUDY_MODE_STATE eStudyModeState=ELMS_STARTUP;
EXAM_MODE_STATE eExamModeState=EXM_STARTUP;
MUSIC_MODE_STATE eMusicModeState=MUSIC_STARTUP;
TEST_MODE_STATE eTestModeState=TEST_STARTUP;

user_sys_state_t main_state=MAIN_POWER_CHECK;

void helpFun(void)
{
  audio_Interrupt_Reset(key_interrupt);
  switch(user_mode_last)
  {
  	case MODE_FREE:
		if(audio_interrupt_flag&key_interrupt)
		{
			switch(eFreeModeState)
			{
				case FMS_CHECK_USART:
				case FMS_AUDIO_PLAY:
					SET_RGB(RGB_CARD_SEL, NONE);
					input_param.insert_Char=0;
					input_param.dataflag=STATUS_INSERT;
					eFreeModeState=FMS_CHECK_USART;
					break;
			}
		}
		break;
		
	case MODE_STUDY:
	case MODE_OL_MYWORD:
		if(audio_interrupt_flag&key_interrupt)
		{
			switch(eStudyModeState)
			{
				case ELMS_WAIT5S:
					SET_RGB(RGB_CARD_SEL, NONE);
					eStudyModeState = ELMS_REREAD;
					break;
					
				case ELMS_CHECK_USART:
					eStudyModeState = ELMS_FIND_LETTER;
					break;
			}
		}
		break;
		
	case MODE_EXAM:
	case MODE_OL_EXAM:
	case MODE_OL_BATTLE:
		if(audio_interrupt_flag&key_interrupt)
		{
			switch(eExamModeState)
			{
				case EXM_CHECK_USART:
				case EXM_AUDIO_PLAY:
					SET_RGB(RGB_CARD_SEL, NONE);
					eExamModeState = EXM_JUMOPVER;
					break;
				default:
					NRF_LOG_DEBUG("in error state:%d",eExamModeState);
					break;
			}
		}
		break;
		
	case MODE_MUSIC:
		audio_force_Interrupt();
		setSongIdx(0,1);//音频序号自加1
		eMusicModeState = MUSIC_PLAYSONG;
		break;
  }
}