#include "common.h"

bool isNewWord(uint16_t WordIndex)
{
    uint16_t ByteIndex;
    uint8_t BitIndex;
    uint16_t WordIndexTmp;
	static uint8_t RepeatCont[(AUDIO_PY_MAP_CODE_LEN) / 8 + 1];
	static bool firstTime=false;
	if(!firstTime)
	{
		firstTime=true;
		memset(RepeatCont,0,sizeof(RepeatCont));
	}
    WordIndexTmp = WordIndex;
    if (WordIndexTmp >= AUDIO_PY_MAP_CODE_LEN)
    {
        return false;
    }
    ByteIndex = WordIndexTmp / 8;
    BitIndex = WordIndexTmp % 8;
    if (RepeatCont[ByteIndex] & (1 << BitIndex))
    {
        return false;
    }
    else
    {
        RepeatCont[ByteIndex] |= 1 << BitIndex;
        return true;
    }
}

bool modeFreePlaying=false;
void FreeMode(FREE_MODE_STATE *psState)
{
    static FREE_MODE_STATE free_state_ret=FMS_STARTUP;
    static uint16_t wordIdx;
	static uint16_t NewWordLearnedCont=0;
	uint8_t py_idx;
    switch (*psState)
    {
        case FMS_STARTUP:
            NRF_LOG_INFO("free mode\r\n");
	    audio_interrupt_flag=all_interrupt;
            audioPush(AUDIO_FREE_MODE);
            *psState=FMS_AUDIO_PLAY;
            free_state_ret=FMS_CHECK_USART_CLEAR;
            break;
            
        case FMS_CHECK_USART_CLEAR:
	    	audio_interrupt_flag|=all_interrupt;
            *psState=FMS_CHECK_USART;
            break;
			
        case FMS_CHECK_USART:
            if(input_param.dataflag==STATUS_INSERT)
            {
            	input_param.dataflag=STATUS_NONE;
            	//if(getWordStr())
            	if(1)
            	{
					if((input_param.word_str_len>1)&&isPyCard(input_param.insert_Char))
					{
						audioPush(audio_26node[input_param.insert_Char-'A']);
						*psState=FMS_AUDIO_PLAY;
		            	free_state_ret=FMS_PLAY_WORD;
					}
            		else
            		{
            			*psState=FMS_PLAY_WORD;
            		}
            	}
				else
				{
					if(!input_param.insert_Char)
						audioPush(AUDIO_InsertThenRead);
				}
            }
//            else if(input_param.dataflag==STATUS_REMOVE)
//            {
//            	*psState=FMS_PLAY_WORD;
//            	input_param.dataflag=STATUS_NONE;
//            }
            break;  
            
        case FMS_PLAY_WORD:
			audio_interrupt_flag=all_interrupt;
	    *psState=FMS_CHECK_USART;
        wordIdx = getWordData(input_param.word_str, input_param.word_str_len);
		if(wordIdx!=0xffff)
		{
			NRF_LOG_INFO("word idx:%d\r\n",wordIdx);
			//找到对应内容
			app_int_fifo_put(&user_param.freeHisFifo, wordIdx);
			if(getCurrentPyLearn(wordIdx))
			{
				if(setSubWordPlayList())
				{
					modeFreePlaying=true;
					free_state_ret=FMS_MEDAL;
					*psState=FMS_AUDIO_PLAY;
				}
			}
		}
		else
		{
			py_idx=isSpecialPY(input_param.word_str);
			switch(py_idx)
			{
				case 0:
					break;
				case 1:
					input_param.word_str[0]='V';
					*psState=FMS_PLAY_WORD;
					break;
				default:
					audioPush(audio_py_error(py_idx-2));
					*psState=FMS_AUDIO_PLAY;
		            free_state_ret=FMS_CHECK_USART;
					break;
			}
		}
            
            break;
            
        case FMS_MEDAL:
		   *psState = FMS_CHECK_USART_CLEAR;	
		   if(isNewWord(wordIdx))
		   {
		   		if (++NewWordLearnedCont >= 5)
	           {
	               NewWordLearnedCont = 0;
	               user_param.Loc_Star += 1;
	               setModify(RECORD_USER_PARAM);
					audioPush(AUDIO_EncourageAndFlower1+rand() % 3);
					audio_play_star();
	               free_state_ret = FMS_CHECK_USART_CLEAR;
	               *psState = FMS_AUDIO_PLAY;
	           }
		   }
           break;
           
        case FMS_AUDIO_PLAY:
			if(input_param.dataflag==STATUS_INSERT)
		   {
		   		*psState = FMS_CHECK_USART_CLEAR;
				modeFreePlaying=false;
				break;
		   }
          if(mp3RecStatus==empty)
          {
            *psState=free_state_ret;
			modeFreePlaying=false;
          }
          break;
          
        default:
            *psState = FMS_STARTUP;
            break;
    }
}