#include "common.h"
#include "boards.h"
#include "bsp.h"
#include "app_fifo.h"
/* button */
#define user_input_buffer_size 16
APP_TIMER_DEF(m_card_read_timer_id);
app_fifo_t cardFifo;

static uint8_t cardButton[user_input_buffer_size];
static const uint8_t m_ir_list[IRS_NUMBER] = IRS_LIST;
static const uint8_t m_com_sel_list[SEL_NUMBER] = CARD_SEL_LIST;
//static char inputBuffer[CARDS_NUMBER];
INPUT_PARAM input_param;

bool user_ir_state_get(uint8_t ir_idx)
{
    ASSERT(ir_idx < IRS_NUMBER);
    bool pin_set = nrf_gpio_pin_read(m_ir_list[ir_idx]) ? true : false;
    return (pin_set == (IR_ACTIVE_STATE ? true : false));
}

void user_card_sel(uint8_t card_idx)
{
  uint8_t idx=card_idx&0x07;
  ASSERT(idx < CARDS_NUMBER);
  idx=5-idx;//取反
  spw_en();
  if(idx&0x01)
    nrf_gpio_pin_set(m_com_sel_list[0]);
  else
    nrf_gpio_pin_clear(m_com_sel_list[0]);
  if(idx&0x02)
    nrf_gpio_pin_set(m_com_sel_list[1]);
  else
    nrf_gpio_pin_clear(m_com_sel_list[1]);
  if(idx&0x04)
    nrf_gpio_pin_set(m_com_sel_list[2]);
  else
    nrf_gpio_pin_clear(m_com_sel_list[2]);
  
}

void user_card_unsel(void)
{
  spw_dis();
  nrf_gpio_pin_set(m_com_sel_list[0]);
  nrf_gpio_pin_set(m_com_sel_list[1]);
  nrf_gpio_pin_set(m_com_sel_list[2]);
}

uint8_t card_data_read(void)
{
  uint8_t i,res=0;
  for(i=0;i<IRS_NUMBER;i++)
  {
    res<<=1;
    res|=user_ir_state_get(i);
  }
  return res;
}

static bool reading=false;
void checkCardInput(void)
{
  uint8_t val;
  static uint8_t sel;

  if(reading)
  {
  	return ;
  }
  if(app_fifo_get(&cardFifo, &val)==NRF_SUCCESS)
  {
      sel=val-BSP_EVENT_KEY_INS_1;
      NRF_LOG_INFO("sel : %x\r\n",sel);
      if(bsp_button_is_pressed(sel))//插入
      {
      	  test_pin();
          user_card_sel(sel);
		  reading=true;
          app_timer_start(m_card_read_timer_id, APP_TIMER_TICKS(50, APP_TIMER_PRESCALER), &sel);
      }
      else
      {
      	input_param.dataflag=STATUS_REMOVE;
        input_param.charBuff[sel]=0;
		input_param.insert_Char=0;
	if((audio_interrupt_flag&insert_interrupt)&&(!modeFreePlaying))
	{
		  SET_RGB(idx2rgb(sel), NONE);
	}
    NRF_LOG_INFO("remove : %d\r\n",sel);
	SEGGER_RTT_printf_BUFFER("char buff",(uint8_t *)input_param.charBuff,CARDS_NUMBER);
      }
  }
}

const char node_table[] = {'1', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 0, 'W', 'X', 'Y', 'Z', 'V', 0, 0, 0, '2'};
static void user_card_check_handler(void * p_context)
{
  uint8_t irs,idx;
  uint8_t *p = (uint8_t *)(unsigned long)p_context;
  idx=p[0];
  audio_Interrupt_Reset(insert_interrupt);
  irs=card_data_read();
  user_card_unsel();
  input_param.charBuff[idx]=node_table[irs];
  input_param.insert_Char=input_param.charBuff[idx];
  input_param.Insert_Pos=idx;
  input_param.dataflag=STATUS_INSERT;
  reading=false;
  checkIfTestCard();
  if((tsetModeFlag == 0x01)||(tsetModeFlag == 0x02))
  {
		SET_RGB(RGB_ALL_SEL, COL_YELLOW);
		main_state=MAIN_STATE_TEST_STARTUP;
   }
  NRF_LOG_INFO("ins: %d\r\n",idx);
  SEGGER_RTT_printf_BUFFER("char buff",(uint8_t *)input_param.charBuff,CARDS_NUMBER);
}

void card_init(void)
{
	uint8_t i;
  memset(&input_param,0,sizeof(INPUT_PARAM));
  uint32_t err_code = app_timer_create(&m_card_read_timer_id,
                                APP_TIMER_MODE_SINGLE_SHOT,
                                user_card_check_handler);
   APP_ERROR_CHECK(err_code);
   app_fifo_init(&cardFifo, cardButton, user_input_buffer_size);
   for(i = 0; i < IRS_NUMBER; ++i)
    {
        nrf_gpio_cfg_input(m_ir_list[i], NRF_GPIO_PIN_NOPULL);
    }
    for(i = 0; i < SEL_NUMBER; ++i)
    {
    	nrf_gpio_cfg_output(m_com_sel_list[i]);
    }
	for(i=0;i<CARDS_NUMBER;i++)
	{
		if(bsp_button_is_pressed(i))
	    {
	    	app_fifo_put(&cardFifo,BSP_EVENT_KEY_INS_1+i);
	    }
	}
}
